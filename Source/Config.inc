//////////////////////////////////////
///  Lina Configuration File       ///
///  ****************************  ///
///  (c) 2015 Dennis G�hlert a.o.  ///
//////////////////////////////////////

{$DEFINE LINA}
{ Globale Einstellungen }
{$DEFINE ADD_SPLASHENTRY}
{$DEFINE ADD_ABOUTENTRY}
{$DEFINE ADD_COMPONENTREG}
{$DEFINE ADD_SINGLECATEGORY}
{$DEFINE WARN_INCOMPATIBLEPLATFORM}
{$DEFINE WARN_INCOMPATIBLECOMPILER}
{$DEFINE WARN_INCOMPATIBLEVERSION}
{ Globale Einschr�nkungen }
{$IFDEF CONDITIONALEXPRESSIONS}
  {$IF (NOT Defined(DCC)) AND Defined(WARN_INCOMPATIBLECOMPILER)}
    { Nur der Delphi-Compiler wird (offiziell) unterst�tzt. }
    {$MESSAGE ERROR 'Lina Components requires Delphi'}
  {$IFEND}
  {$IF (NOT Defined(MSWINDOWS)) AND Defined(WARN_INCOMPATIBLECOMPILER)}
    { Nur Windows wird (offiziell) als Ziel-Platform unterst�tzt. }
    {$MESSAGE ERROR 'Lina Components requires Microsoft Windows'}
  {$IFEND}
  {$IF (CompilerVersion < 15.0) AND Defined(WARN_INCOMPATIBLEVERSION)}
    { Fr�here Delphi-Versionen als Delphi 7 werden nicht (offiziell)
      unterst�tzt. }
    {$MESSAGE ERROR 'Lina Components requires Delphi 7 or higher'}
  {$IFEND}
  {$IF CompilerVersion < 17.0}
    { Unter fr�heren Delphi-Versionen als 2005 gab es noch nicht die
      M�glichkeit, Records mit methoden zu versehen. Die Implementierung von
      Prozeduren und/oder Funktionen war Klassen vorenthalten. }
    {$DEFINE NO_RECORDMETHODS}
  {$IFEND}
  {$IF CompilerVersion < 18.5}
    { Unter fr�heren Delphi-Versionen als 2007 gab es (offiziell) noch keine
      Unterst�tzung f�r Windows-Vista-spezifische funktionen wie die TaskDialog-
      Komponente oder einige Funktionen.
      Damit dies nicht zu Problemen f�hrt, geht der Compiler darauf mithilfe
      dieser Direktive ein. }
    {$DEFINE NO_VISTA}
  {$IFEND}
  {$IF CompilerVersion < 20.0}
    { Unter fr�heren Delphi-Versionen als 2009 war die Nutzung generischer
      Datentypen (Generics) nicht m�glich. Da es ohne diese nahezu unm�glich
      scheint, typisierte Objekte, wie zum Beispiel typisierte TObjectList's
      zu deklarieren, fallen diese Deklarationen unter fr�heren Delphi-Versionen
      weg. Dies kann eventuell zu kompatibilit�tsproblemen zwischen
      unterschiedlichen Nutzern dieser Komponenten f�hren, l�sst sich aber nicht
      vermeiden. }
    {$DEFINE NO_GENERIC}
    { Unter fr�heren Delphi-Versionen als 2009 entsprach der String-Typenalias
      dem AnsiString-Typen. Da es gegebenfalls bei manchen externen (zB. OS-
      Spezifischen) Klassen zu Kompatibilit�tsproblemen kommmen k�nnte, wird
      hier eine �berpr�fung der Unicode-Unterst�tzung durchgef�hrt.
      Zu beachten ist jedoch, dass Delphi 2009 und h�her AnsiStrings implizit
      in UnicodeStrings umwandelt und umgekehrt (sofern m�glich). }
    {$DEFINE NO_UNICODE}
  {$IFEND}
  {$IF CompilerVersion < 21.0}
    { Unter fr�heren Delphi-Versionen als 2010 gab es ein Problem mit dem
      "crHandPoint" cursor, sodass dieser nicht korrekt unter Windows Vista oder
      h�her dargestellt wurde, sondern stattdessen im alten Windows-Look.
      Bei �lteren Delphi-Versionen als 2010 steht deswegen eine Pseudo-
      Komponente zur Behebung dieses Problems zur Verf�gung. }
    {$DEFINE NO_HANDPOINT}
  {$IFEND}
  {$IF CompilerVersion < 23.0}
    { Unter fr�heren Delphi-Versionen als XE2 stand lediglich eine Ziel-Platform
      f�r kompilierte Anwendungen zur Verf�gung. Deshalb brauchte noch nicht auf
      Kompatibilit�t zu mehrfacher Platform-Unterst�tzung geachtet werden. }
    {$DEFINE NO_MULTIPLATFORM}
  {$IFEND}
{$ENDIF}
