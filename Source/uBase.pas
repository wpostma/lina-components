unit uBase;

//////////////////////////////////////
///  Lina Base Unit                ///
///  ****************************  ///
///  (c) 2016 Dennis G�hlert a.o.  ///
//////////////////////////////////////

  {$I 'Config.inc'}

interface

  { Dies ist die Basis-Unit f�r die Lina-Komponenten bzw. Methoden und Klassen.
    Diese Unit soll nur indirekt �ber Lina-Komponenten und ggf. Klassen
    eingebunden werden! }

uses
  { Standard-Units }
  SysUtils, Classes, Dialogs, uSysTools;

const
  UnitName = 'uBase';
  LinaVersion = 1.00;
  About_Title = 'About...';
  { Komponenten-Informationen }
  ComponentsPage = 'Lina';
  ComponentsVersion = '1.0';
  ComponentsCopyright = 'Copyright � 2016';
  ComponentsAuthor = 'Dennis G�hlert a.o.';
  ComponentsHomepage = '';

type
  TComponentAbout = class
  private
    { Private-Deklarationen }
    FComponent: TComponentClass;
    FName: TComponentName;
    FVersion: ShortString;
    FAuthor: ShortString;
    FCopyright: ShortString;
    FHomepage: ShortString;
  protected
    { Protected-Deklarationen }
    property Component: TComponentClass read FComponent write FComponent;
  published
    { Published-Deklarationen }
    property Name: TComponentName read FName;
    property Version: ShortString read FVersion;
    property Copyright: ShortString read FCopyright;
    property Author: ShortString read FAuthor;
    property Homepage: ShortString read FHomepage;
  public
    { Public-Deklarationen }
    constructor Create(Component: TComponentClass; Ver:
      ShortString = ComponentsVersion; Copy: ShortString = ComponentsCopyright;
      Auth: ShortString = ComponentsAuthor;
      Home: ShortString = ComponentsHomepage);
    { �ber-Dialog }
    procedure AboutDlg;
  end;

implementation

constructor TComponentAbout.Create(Component: TComponentClass;
  Ver: ShortString = ComponentsVersion; Copy: ShortString = ComponentsCopyright;
  Auth: ShortString = ComponentsAuthor; Home: ShortString = ComponentsHomepage);
begin
  FComponent := Component;
  FName := ExtractClassName(Component.ClassName);
  FVersion := Ver;
  FCopyright := Copy;
  FAuthor := Auth;
  FHomepage := Homepage;
end;

procedure TComponentAbout.AboutDlg;
begin
  {$IFDEF NO_VISTA}
    { MessageDlg, falls der Compiler KEINE Vista-Dialoge unterst�tzt }
    MessageDlg(
  {$ELSE}
    { TaskMessageDlg, falls der Compiler Vista-Dialoge unterst�tzt }
    TaskMessageDlg(
  {$ENDIF}
                   About_Title,
                 { ---------------------------------- }
                   Name + ' v'
                 + Version + sLineBreak
                 + Copyright + ' ' + Author + sLineBreak
                 + Homepage,
                 { ---------------------------------- }
                   mtInformation,
                 { ---------------------------------- }
                   [mbClose],0)
end;

end.
