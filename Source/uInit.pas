unit uInit;

//////////////////////////////////////
///  Lina Initialization Unit      ///
///  ****************************  ///
///  (c) 2016 Dennis G�hlert a.o.  ///
//////////////////////////////////////

  {$I 'Config.inc'}

interface

  { Dies ist die Initialisierungs-Unit f�r das Lina Components Package.
    Diese Unit darf NIEMALS direkt in eine Laufzeit-Bibliothek eingebunden
    werden! }

uses
  { Standard-Units }
  ToolsAPI, Graphics;

  procedure RegisterPackageOnSplash;
  procedure RegisterPackageOnAbout;
  procedure UnregisterPackageOnAbout;

const
  Package_Name = 'Lina Components';
  Package_Description = 'Components and code library for Delphi' + sLineBreak + '� 2015 Dennis G�hlert a.o.';
  Package_License = 'Mozilla Public License (MPL) 2.0';
  Package_SKU = '(Dev-Preview)';

var
  PluginIndex: Integer = -1;
  AboutBitmap: TBitmap;

implementation

procedure RegisterPackageOnSplash;
var
  SplashBitmap: TBitmap;
begin
  SplashBitmap := TBitmap.Create;
  try
    SplashBitmap.LoadFromResourceName(HInstance,'LINA');
    (SplashScreenServices as IOTASplashScreenServices).AddPluginBitmap(
     Package_Name,SplashBitmap.Handle,False,Package_License,Package_SKU);
  finally
    SplashBitmap.Free;
  end;
end;

procedure RegisterPackageOnAbout;
begin
  AboutBitmap := TBitmap.Create;
  try
    AboutBitmap.LoadFromResourceName(HInstance,'LINA');
    PluginIndex := (BorlandIDEServices as IOTAAboutBoxServices120).AddPluginInfo(
    Package_Name,Package_Description,AboutBitmap.Handle,False,Package_License,Package_SKU);
  except
    AboutBitmap.Free;
  end;
end;

procedure UnregisterPackageOnAbout;
begin
  try
    (BorlandIDEServices As IOTAAboutBoxServices).RemovePluginInfo(PluginIndex);
  finally
    AboutBitmap.Free;
  end;
end;

initialization
  { Package-Registrierung }
  {$IFDEF ADD_SPLASHENTRY}
    RegisterPackageOnSplash;
  {$ENDIF}
  {$IFDEF ADD_ABOUTENTRY}
    RegisterPackageOnAbout;
  {$ENDIF}


finalization
  { Package-Deregistrierung }
  {$IFDEF ADD_ABOUTENTRY}
    UnregisterPackageOnAbout;
  {$ENDIF}

end.
