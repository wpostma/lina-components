unit uWebCtrls;

//////////////////////////////////////
///  Lina Web Controls Unit        ///
///  ****************************  ///
///  (c) 2016 Dennis G�hlert a.o.  ///
//////////////////////////////////////

  {$I 'Config.inc'}

interface

uses
  { Standard-Units }
  SysUtils, Classes,
  { Indy-Units }
  idHTTP, idSSLOpenSSL, idComponent,
  { Andere Package-Units }
  uBase, uSysTools;

type
  { Fehlermeldungen }
  EInvalidWebAddress = class(Exception);
  EInvalidTagChar = class(Exception);

type
  { Ereignisse }
  TDownloadWorkEvent = procedure(Sender: TObject; AWorkMode: TWorkMode) of object;
  TDownloadWorkBeginEvent = procedure(Sender: TObject; AWorkMode: TWorkMode) of object;
  TDownloadWorkEndEvent = procedure(Sender: TObject; AWorkMode: TWorkMode) of object;

type
  { Hauptklassen }
  {$IFNDEF NO_MULTIPLATFORM}
    [ComponentPlatformsAttribute(pidWin32 or pidWin64)]
  {$ENDIF}
  TDownload = class(TComponent)
  private
    { Private-Deklarationen }
    idHTTPObject: TidHTTP;
    SSLHandler: TIdSSLIOHandlerSocketOpenSSL;
    FAbout: TComponentAbout;
    FAddress: String;
    FProgress: Int64;
    FProgressMax: Int64;
    FSSL: Boolean;
    { Ereignisse }
    FWorkEvent: TDownloadWorkEvent;
    FWorkBeginEvent: TDownloadWorkBeginEvent;
    FWorkEndEvent: TDownloadWorkEndEvent;
    { Methoden }
    procedure SetAddress(Value: String);
    procedure idHTTPObjectWork(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
    procedure idHTTPObjectWorkBegin(ASender: TObject; AWorkMode: TWorkMode; AWorkCountMax: Int64);
    procedure idHTTPObjectWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
  protected
    { Protected-Deklarationen }
    procedure Prepare;
  public
    { Public-Deklarationen }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    { Fortschritt }
    function GetProgress: Int64;
    function GetProgressMax: Int64;
    function GetProgressPercent: Byte;
    { Herunterladen }
    function GetText: String;
    procedure SaveToFile(const FileName: TFileName);
    procedure SaveToStream(var Stream: TStream);
  published
    { Published-Deklarationen }
    { Ereignisse }
    property OnWork: TDownloadWorkEvent read FWorkEvent write FWorkEvent;
    property OnWorkBegin: TDownloadWorkBeginEvent read FWorkBeginEvent write FWorkBeginEvent;
    property OnWorkEnd: TDownloadWorkEndEvent read FWorkEndEvent write FWorkEndEvent;
    { Eigenschaften }
    property About: TComponentAbout read FAbout;
    property Address: String read FAddress write SetAddress;
    property SSL: Boolean read FSSL write FSSL default False;
  end;

  function ValidProtocol(const Protocol: String; const Protocols: array of String): Boolean;
  function StrIsURL(const S: String): Boolean;
  function GetTagParamValue(const S,Tag,Param: String): String;

  {$IFDEF ADD_COMPONENTREG}
    procedure Register;
  {$ENDIF}

const
  { Web-Protokolle }
  WP_HTTP = 'http://';
  WP_HTTPS = 'https://';
  WP_FTP = 'ftp://';
  WP_CALL = 'callto:';
  WP_MAIL = 'mailto:';
  WebProtocols: array [0..4] of String = (WP_HTTP,WP_HTTPS,WP_FTP,WP_CALL,WP_MAIL);
  WebProtocolsSimple: array [0..1] of String = (WP_MAIL,WP_CALL);
  WebProtocolsSpecial: array [0..2] of String = (WP_HTTP,WP_HTTPS,WP_FTP);

implementation

{$IFDEF ADD_COMPONENTREG}
  procedure Register;
  begin
    RegisterComponents(ComponentsPage,[TDownload]);
  end;
{$ENDIF}

function ValidProtocol(const Protocol: String;
  const Protocols: array of String): Boolean;
var
  Index: 1..5;
begin
  Result := (ArrayPos(Protocol,Protocols) <> -1);
end;

function StrIsURL(const S: String): Boolean;
{ Ein einfacher (iterativer) Parser, der �berpr�fen soll, ob ein String die
  Anforderungen an eine URL-Internetadresse erf�llt.
  Hier sei gesagt, dass dies KEINE vollst�ndige Pr�fung ist, sondern nur die
  wichtigsten Kriterien beinhaltet.
  Au�erdem wird auch nicht �berpr�ft, ob eine Adresse verf�gbar ist. }
const
  //Diese Zeichen sind zwar erlaubt, d�rfen jedoch nicht doppelt hintereinander vorkommen
  InvalidDoubleChars = [':','.'];
var
  Index: Integer;
  DomainLength: Integer;
  Protocol: String;
  ProtocolValid: Boolean;
  DoubleSlashRequired: Boolean;
begin
  Result := True;
  ProtocolValid := False;
  DoubleSlashRequired := False;
  DomainLength := 0;
  Protocol := '';
  for Index := 1 to Length(S) do
  begin
    if (S[Index] in InvalidDoubleChars) and
       (S[Index] = S[Index - 1]) then
    begin
      Result := False;
      Exit;
    end;
    if (S[Index] in Spaces) or
       ((S[Index] = ':') and (ProtocolValid = True)) then
    begin
      Result := False;
      Exit;
    end;
    if ProtocolValid = False then
    begin
      Protocol := Protocol + S[Index];
      if S[Index] = ':' then
      begin
        ProtocolValid := True;
        if ValidProtocol(Protocol,WebProtocolsSimple) = False then
        begin
          DoubleSlashRequired := True;
          Continue;
        end;
      end;
      if S[Index] = '/' then
      begin
        Result := False;
        Exit;
      end;
    end else
    begin
      if S[Index] = '/' then
      begin
        if DoubleSlashRequired = True then
        begin
          Protocol := Protocol + S[Index];
          DoubleSlashRequired := False;
        end else
        begin
          if S[Index - 1] = '/' then
          begin
            if ValidProtocol(Protocol + S[Index],WebProtocolsSpecial) = False then
            begin
              Result := False;
              Exit;
            end else
            begin
              Protocol := Protocol + S[Index];
            end;
          end;
        end;
      end else
      begin
        if (DoubleSlashRequired = True) or ((S[Index - 1] = '/') and (S[Index - 2] <> '/') and (DomainLength = 0)) then
        begin
          Result := False;
          Exit;
        end else
        begin
          DomainLength := DomainLength + 1;
        end;
      end;
    end;
  end;
  Result := (ProtocolValid and (DomainLength > 3));
end;

function GetTagParamValue(const S,Tag,Param: String): String;
{ Ziemlich schneller XML/HTML-Parser, der nach einem Tag und einem dort
  enthaltenem Parameter sucht. Der Wert dieses Parameters wird dann
  als Ergebnis der Funktion zur�ckgegeben. }
var
  Finished: Boolean;
  Current: PChar;
  InTag,InValue: Boolean;
  Ignore: Boolean;
  Equal: Boolean;
  Block: String;
begin
  Result := '';
  if (Length(S) < 4) or (Length(Tag) = 0) or (Length(Param) = 0) then
  begin
    Exit;
  end;
  Finished := False;
  InTag := False;
  InValue := False;
  Ignore := True;
  Equal := False;
  Block := '';
  Current := @S[1];
  while True do
  begin
    if InTag = True then
    begin
      if InValue = True then
      begin
        if Current^ = '"' then
        begin
          InValue := False;
          if Ignore = False then
          begin
            Result := Block;
            Exit;
          end;
          Block := '';
          Equal := False;
        end else
        begin
          Block := Block + Current^;
        end;
        //-->
        Inc(Current);
        Continue;
      end;
      if Equal = True then
      begin
        if Current^ in Spaces then
        begin
          //-->
          Inc(Current);
          Continue;
        end;
        if Current^ = '"' then
        begin
          InValue := True;
          Ignore := not ((Ignore = True) and (Block = Param));
          Block := '';
          //-->
          Inc(Current);
          Continue;
        end;
      end else
      begin
        if Current^ in Letters + Numbers then
        begin
          Block := Block + Current^;
          //-->
          Inc(Current);
          Continue;
        end;
        if Current^ = '>' then
        begin
          InTag := False;
          Block := '';
          //-->
          Inc(Current);
          Continue;
        end;
        if Current^ in Spaces then
        begin
          if Block = Tag then
          begin
            Ignore := False;
          end;
          Block := '';
          //-->
          Inc(Current);
          Continue;
        end;
        if Current^ = '=' then
        begin
          Equal := True;
          //-->
          Inc(Current);
          Continue;
        end;
      end;
    end else
    begin
      if Current^ = '<' then
      begin
        InTag := True;
      end;
      //-->
      Inc(Current);
      Continue;
    end;
    raise EInvalidTagChar.Create('Invalid character: "' + Current^ + '"');
    Exit;
  end;
end;

{ ----------------------------------------------------------------------------
  TDownload
  ---------------------------------------------------------------------------- }

constructor TDownload.Create(AOwner: TComponent);
begin
  inherited;
  FAbout := TComponentAbout.Create(TDownload);
  idHTTPObject := TidHTTP.Create(Self);
  idHTTPObject.HandleRedirects := True;
  idHTTPObject.OnWork := idHTTPObjectWork;
  idHTTPObject.OnWorkBegin := idHTTPObjectWorkBegin;
  idHTTPObject.OnWorkEnd := idHTTPObjectWorkEnd;
  SSLHandler := TIdSSLIOHandlerSocketOpenSSL.Create(Self);
  SSL := False;
end;

destructor TDownload.Destroy;
begin
  FAbout.Free;
  inherited;
end;

procedure TDownload.SetAddress(Value: String);
begin
  if (StrIsURL(Value) = True) or (Length(Value) = 0) then
  begin
    FAddress := Value;
  end else
  begin
    raise EInvalidWebAddress.Create('"' + Value + '" is not a valid URL address');
  end;
end;

procedure TDownload.idHTTPObjectWork(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
begin
  FProgress := AWorkCount;
  if Assigned(OnWork) then
  begin
    OnWork(Self,AWorkMode);
  end;
end;

procedure TDownload.idHTTPObjectWorkBegin(ASender: TObject; AWorkMode: TWorkMode; AWorkCountMax: Int64);
begin
  FProgressMax := AWorkCountMax;
  if Assigned(OnWorkBegin) then
  begin
    OnWorkBegin(Self,AWorkMode);
  end;
end;

procedure TDownload.idHTTPObjectWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
begin
  if Assigned(OnWorkEnd) then
  begin
    OnWorkEnd(Self,AWorkMode);
  end;
end;

function TDownload.GetProgress: Int64;
begin
  Result := FProgress;
end;

function TDownload.GetProgressMax: Int64;
begin
  Result := FProgressMax;
end;

function TDownload.GetProgressPercent: Byte;
begin
  if FProgressMax > 0 then
  begin
    Result := (FProgress div FProgressMax) * 100;
  end else
  begin
    { Bei aktivierter SSL-Verschl�sselung wird keine insgesamte Dateigr��e
      �bermittelt. Deshalb wird hier eine 0 angegeben, um das Ergebnis nicht
      undefiniert zu lassen. }
    Result := 0;
  end;
end;

function TDownload.GetText: String;
begin
  Prepare;
  Result := idHTTPObject.Get(Address);
end;

procedure TDownload.SaveToFile(const FileName: TFileName);
var
  FS: TFileStream;
begin
  Prepare;
  FS := TFileStream.Create(FileName,fmCreate or fmShareDenyWrite);
  try
    idHTTPObject.Get(Address,FS);
  finally
    FS.Free;
  end;
end;

procedure TDownload.SaveToStream(var Stream: TStream);
begin
  Prepare;
  idHTTPObject.Get(Address,Stream);
end;

procedure TDownload.Prepare;
begin
  if SSL = True then
  begin
    idHTTPObject.IOHandler := SSLHandler;
  end else
  begin
    idHTTPObject := nil;
  end;
end;

end.
