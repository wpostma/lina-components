unit uSysTools;

//////////////////////////////////////
///  Lina System Tools Unit        ///
///  ****************************  ///
///  (c) 2016 Dennis G�hlert a.o.  ///
//////////////////////////////////////

  {$I 'Config.inc'}
  {$POINTERMATH ON}

interface

uses
  { Standard-Units }
  SysUtils, Classes, Math, Windows, Graphics, Printers, TypInfo
  {$IFNDEF NO_GENERIC}
    ,Generics.Collections
  {$ENDIF}
  ;

type
  { Fehlermeldungen }
  EWinUserInformation = class(Exception);
  EStringCharAccess = class(Exception);
  EInvalidBFCommand = class(Exception);

type
  { Hilfsklassen }
  TLinePosition = (lpAnyPosition,lpBeginning); //Mutter-Hilfsklasse f�r s�mtliche Enums
  TStringFilterMode = type TLinePosition;
  TStringFilterOptions = set of (sfoCaseSensitive,sfoForceTrim,sfoDefaultVisible);
  TCharEncoding = (ceANSI,ceASCII,ceBigEndianUnicode,ceUnicode,ceUTF7,ceUTF8);
  {$IF !Declared(TVerticalAlignment}
    TVerticalAlignment = (taAlignTop, taAlignBottom, taVerticalCenter);
  {$ENDIF}

type
  {$IFNDEF NO_GENERIC}
    TVariantList = TList<Variant>;
    TIntegerList = TList<Integer>;
    TBooleanList = TList<Boolean>;
    TFloatList = TList<Extended>;
    TPointList = TList<TPoint>;
  {$ENDIF}
  { Typisierte Arrays }
  TPointerArray = array of Pointer;
  TPCharArray = array of PChar;
  TCharArray = array of Char;

  TVariantArray = array of Variant;

  TStringArray = array of String;
  TShortStringArray = array of ShortString;
  {$IFDEF NO_UNICODE}
    TAnsiStringArray = TStringArray;
  {$ELSE}
    TAnsiStringArray = array of AnsiString;
    TUnicodeStringArray = TStringArray;
  {$ENDIF}

  TByteArray = array of Byte;
  TUInt8Array = TByteArray;
  TWordArray = array of Word;
  TUInt16Array = TWordArray;
  TCardinalArray = array of Cardinal;
  TUInt32Array = TCardinalArray;
  TUInt64Array = array of UInt64;

  TShortIntArray = array of ShortInt;
  TInt8Array = TShortIntArray;
  TSmallIntArray = array of SmallInt;
  TInt16Array = TSmallIntArray;
  TIntegerArray = array of Integer;
  TInt32Array = TIntegerArray;
  TLongIntArray = TIntegerArray;
  TInt64Array = array of Int64;

  TBooleanArray = array of Boolean;
  TByteBoolArray = array of ByteBool;
  TWordBoolArray = array of WordBool;
  TLongBoolArray = array of LongBool;

  TFloatArray = array of Extended;
  TSingleArray = array of Single;
  TDoubleArray = array of Double;
  TRealArray = TDoubleArray;
  TExtendedArray = TFloatArray;

  TPointArray = array of TPoint;

  TReferenceData = record
    Value: Pointer;
    Reference: PPointer;
  end;
  TReferenceDataArray = array of TReferenceData;
  PReferenceData = ^TReferenceData;
  PReferenceDataArray = ^TReferenceDataArray;
  TRefDataReferenceData = record
    Value: Pointer;
    Reference: PReferenceData;
  end;
  TRefDataReferenceDataArray = array of TRefDataReferenceData;
  TRefDataArrayReferenceData = record
    Value: Pointer;
    Reference: PReferenceDataArray;
  end;
  TRefDataArrayReferenceDataArray = array of TRefDataArrayReferenceData;

  TStringReferenceData = record
    Value: String;
    Reference: PString;
  end;
  TStringReferenceDataArray = array of TStringReferenceData;
  PStringReferenceData = ^TStringReferenceData;
  PStringReferenceDataArray = ^TStringReferenceDataArray;
  TStringRefDataReferenceData = record
    Value: String;
    Reference: PStringReferenceData;
  end;
  TStringRefDataReferenceDataArray = array of TStringRefDataReferenceData;
  TStringRefDataArrayReferenceData = record
    Value: String;
    Reference: PStringReferenceDataArray;
  end;
  TStringRefDataArrayReferenceDataArray = array of TStringRefDataArrayReferenceData;

  TIntegerReferenceData = record
    Value: Integer;
    Reference: PInteger;
  end;
  TIntegerReferenceDataArray = array of TIntegerReferenceData;
  PIntegerReferenceData = ^TIntegerReferenceData;
  PIntegerReferenceDataArray = ^TIntegerReferenceDataArray;
  TIntegerRefDataReferenceData = record
    Value: Integer;
    Reference: PIntegerReferenceData;
  end;
  TIntegerRefDataReferenceDataArray = array of TIntegerRefDataReferenceData;
  TIntegerRefDataArrayReferenceData = record
    Value: Integer;
    Reference: PIntegerReferenceDataArray;
  end;
  TIntegerRefDataArrayReferenceDataArray = array of TIntegerRefDataArrayReferenceData;

  TFloatReferenceData = record
    Value: Extended;
    Reference: PExtended;
  end;
  TFloatReferenceDataArray = array of TFloatReferenceData;
  PFloatReferenceData = ^TFloatReferenceData;
  PFloatReferenceDataArray = ^TFloatReferenceDataArray;
  TFloatRefDataReferenceData = record
    Value: Extended;
    Reference: PFloatReferenceData;
  end;
  TFloatRefDataReferenceDataArray = array of TFloatRefDataReferenceData;
  TFloatRefDataArrayReferenceData = record
    Value: Extended;
    Reference: PFloatReferenceDataArray;
  end;
  TFloatRefDataArrayReferenceDataArray = array of TFloatRefDataArrayReferenceData;

  TByteSet = set of Byte;
  TCharSet = set of Char;

  TVector = TFloatArray;
  TMatrix = array of TVector;

  TCycle = class
  private
    { Private-Deklarationen }
    FRadius: Extended;
    { Methoden }
    function GetDiameter: Extended;
    procedure SetDiameter(Value: Extended);
    function GetCircumference: Extended;
    procedure SetCircumference(Value: Extended);
  public
    { Public-Deklarationen }
    constructor Create;
    destructor Destroy; override;
    property Radius: Extended read FRadius write FRadius;
    property Diameter: Extended read GetDiameter write SetDiameter;
    property Circumference: Extended read GetCircumference write SetCircumference;
  end;

  TRange = class
  private
    { Private-Deklarationen }
    FOffset: Integer;
    FTarget: Integer;
    FStep: Integer;
    { Methoden }
    procedure SetOffset(Value: Integer);
    procedure SetTarget(Value: Integer);
    function GetLength: Integer;
    procedure SetLength(Value: Integer);
    function GetCount: Integer;
    function GetElements: TIntegerArray;
  public
    { Public-Deklarationen }
    constructor Create;
    destructor Destroy; override;
    property Offset: Integer read FOffset write SetOffset default 0;
    property Target: Integer read FTarget write SetTarget default 0;
    property Length: Integer read GetLength write SetLength default 0;
    property Count: Integer read GetCount default 0;
    property Elements: TIntegerArray read GetElements;
    property Step: Integer read FStep write FStep default 1;
  end;

  TFilteredStringList = class(TStringList)
  private
    { Private-Deklarationen }
    FFiltered: Boolean;
    FFilter: String;
    FFilteredStrings: TStrings;
    FFilterMode: TStringFilterMode;
    FFilterOptions: TStringFilterOptions;
    { Methoden }
    function GetFilteredStrings: TStrings;
  protected
    { Protected-Deklarationen }
    procedure FilterUpdate;
  public
    { Public-Deklarationen }
    constructor Create;
    destructor Destroy; override;
    property Filtered: Boolean read FFiltered write FFiltered default True;
    property Filter: String read FFilter write FFilter;
    property FilteredStrings: TStrings read GetFilteredStrings;
    property FilterMode: TStringFilterMode read FFilterMode write FFilterMode default lpBeginning;
    property FilterOptions: TStringFilterOptions read FFilterOptions write FFilterOptions default [sfoCaseSensitive,sfoForceTrim,sfoDefaultVisible];
  end;

  { Typenumwandelungen }
  function BoolToInt(B: Boolean): Integer; inline;
  function IntToBool(Value: Integer): Boolean; inline;
  { Typen-�quivalenz-Pr�fungen }
  function StrIsInt(const S: String): Boolean;
  function StrIsFloat(const S: String): Boolean;
  function StrIsBool(const S: String): Boolean;
  function FloatIsInt(Value: Extended): Boolean;
  { WinUser }
  function WinUserName: String;
  function WinUserDirectory: String;
  function WinUserAdmin: Boolean;
  function WinUserExists(UsrNme: String): Boolean;
  { Array-Position }
  function ArrayPos(const AValue; const AArray: array of const): Integer; overload;
  function ArrayPos(const AValue: Variant; const AArray: array of Variant): Integer; overload;
  function ArrayPos(const AValue: Pointer; const AArray: array of Pointer): Integer; overload;
  function ArrayPos(const AValue: Char; const AArray: array of Char): Integer; overload;
  function ArrayPos(const AValue: ShortString; const AArray: array of ShortString): Integer; overload;
  function ArrayPos(const AValue: String; const AArray: array of String): Integer; overload;
  function ArrayPos(const AValue: ShortInt; const AArray: array of ShortInt): Integer; overload;
  function ArrayPos(const AValue: SmallInt; const AArray: array of SmallInt): Integer; overload;
  function ArrayPos(const AValue: Integer; const AArray: array of Integer): Integer; overload;
  function ArrayPos(const AValue: Int64; const AArray: array of Int64): Integer; overload;
  function ArrayPos(const AValue: Byte; const AArray: array of Byte): Integer; overload;
  function ArrayPos(const AValue: Word; const AArray: array of Word): Integer; overload;
  function ArrayPos(const AValue: Cardinal; const AArray: array of Cardinal): Integer; overload;
  function ArrayPos(const AValue: UInt64; const AArray: array of UInt64): Integer; overload;
  function ArrayPos(const AValue: Single; const AArray: array of Single): Integer; overload;
  function ArrayPos(const AValue: Double; const AArray: array of Double): Integer; overload;
  function ArrayPos(const AValue: Real; const AArray: array of Real): Integer; overload;
  function ArrayPos(const AValue: Extended; const AArray: array of Extended): Integer; overload;
  function ArrayPosRef(const AValue: Pointer; const AArray: array of TReferenceData): Integer; overload;
  function ArrayPosRef(const AValue: String; const AArray: array of TStringReferenceData; IgnoreCase: Boolean = False): Integer; overload;
  function ArrayPosRef(const AValue: Integer; const AArray: array of TIntegerReferenceData): Integer; overload;
  function ArrayPosRef(const AValue: Extended; const AArray: array of TFloatReferenceData): Integer; overload;
  function ArrayPosRef(const AValue: Pointer; const AArray: array of TRefDataArrayReferenceData): Integer; overload;
  function ArrayPosRef(const AValue: String; const AArray: array of TStringRefDataArrayReferenceData; IgnoreCase: Boolean = False): Integer; overload;
  function ArrayPosRef(const AValue: Integer; const AArray: array of TIntegerRefDataArrayReferenceData): Integer; overload;
  function ArrayPosRef(const AValue: Extended; const AArray: array of TFloatRefDataArrayReferenceData): Integer; overload;
  function ArrayPosType(AValue: TClass; AArray: array of TClass): Integer; overload;
  function ArrayPosType(AValue: TClass; AArray: array of TObject): Integer; overload;
  { Array-Element L�schen }
  procedure ArrayDelete(var AArray: TVariantArray; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TCharArray; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TShortStringArray; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TStringArray; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TShortIntArray; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TSmallIntArray; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TIntegerArray; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TInt64Array; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TByteArray; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TWordArray; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TCardinalArray; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TUInt64Array; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TSingleArray; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TDoubleArray; Index: Integer; Count: Integer); overload;
  procedure ArrayDelete(var AArray: TExtendedArray; Index: Integer; Count: Integer); overload;
  { TComponent Laden/Speichern }
  procedure ComponentSaveToFile(const FileName: String; Component: TComponent);
  procedure ComponentLoadFromFile(const FileName: String; Component: TComponent);
  procedure ComponentSaveToStream(var Stream: TStream; Component: TComponent);
  procedure ComponentLoadFromStream(Stream: TStream; Component: TComponent);
  { Char-Case-Umwandelungen }
  function LowCase(Ch: Char): Char;
  function CharLowerCase(Character: Char): Char;
  function CharUpperCase(Character: Char): Char;
  { Null-/Plus-Minus-Unendlich- Ann�herung }
  procedure ToZero(var X: Integer);
  procedure ToInf(var X: Integer);
  { Gradzahlen-Normalisierer }
  procedure NormalizeDeg(var X: Integer); overload;
  procedure NormalizeDeg(var X: Single); overload;
  procedure NormalizeDeg(var X: Double); overload;
  procedure NormalizeDeg(var X: Extended); overload;
  { PQ-Formel }
  function PQFormula(P,Q: Single): TSingleArray; overload;
  function PQFormula(P,Q: Double): TDoubleArray; overload;
  function PQFormula(P,Q: Extended): TExtendedArray; overload;
  { Gleitkomma-Modulo }
  function FloatMod(X,Y: Single): Single; overload;
  function FloatMod(X,Y: Double): Double; overload;
  function FloatMod(X,Y: Extended): Extended; overload;
  { Ganzzahliges Exponenzieren }
  function IntPow(Base: ShortInt; Exponent: Byte): Int64; overload;
  function IntPow(Base: SmallInt; Exponent: Byte): Int64; overload;
  function IntPow(Base: Integer; Exponent: Byte): Int64; overload;
  function IntPow(Base: Int64; Exponent: Byte): Int64; overload;
  { GGT ("GCD") / KGV ("LCM") }
  function GCD(A,B: Byte): Byte; overload;
  function GCD(A,B: Word): Word; overload;
  function GCD(A,B: Cardinal): Cardinal; overload;
  function LCM(A,B: Byte): Word; overload;
  function LCM(A,B: Word): Cardinal; overload;
  function LCM(A,B: Cardinal): Cardinal; overload;
  { Summenformel }
  function SumOf(Value: Byte; Offset: Byte = 1): Word; overload;
  function SumOf(Value: Word; Offset: Word = 1): Cardinal; overload;
  function SumOf(Value: Cardinal; Offset: Cardinal = 1): Cardinal; overload;
  { Addition numerischer Arrays }
  function ArrayAdd(A,B: array of ShortInt): TShortIntArray; overload;
  function ArrayAdd(A,B: array of SmallInt): TSmallIntArray; overload;
  function ArrayAdd(A,B: array of Integer): TIntegerArray; overload;
  function ArrayAdd(A,B: array of Int64): TInt64Array; overload;
  function ArrayAdd(A,B: array of Byte): TByteArray; overload;
  function ArrayAdd(A,B: array of Word): TWordArray; overload;
  function ArrayAdd(A,B: array of Cardinal): TCardinalArray; overload;
  function ArrayAdd(A,B: array of Single): TSingleArray; overload;
  function ArrayAdd(A,B: array of Double): TDoubleArray; overload;
  function ArrayAdd(A,B: array of Extended): TExtendedArray; overload;
  { Sortier-Algorithmen }
  procedure BubbleSort(var Elements: array of String); overload;
  procedure BubbleSort(var Elements: array of ShortInt); overload;
  procedure BubbleSort(var Elements: array of SmallInt); overload;
  procedure BubbleSort(var Elements: array of Integer); overload;
  procedure BubbleSort(var Elements: array of Int64); overload;
  procedure BubbleSort(var Elements: array of Byte); overload;
  procedure BubbleSort(var Elements: array of Word); overload;
  procedure BubbleSort(var Elements: array of Cardinal); overload;
  procedure BubbleSort(var Elements: array of Single); overload;
  procedure BubbleSort(var Elements: array of Double); overload;
  procedure BubbleSort(var Elements: array of Extended); overload;
  { RTTI-Werzeuge }
  function GetSubPropInfo(Instance: TObject; const PropName: String; AKinds: TTypeKinds = []): PPropInfo;
  function GetObjectSubProp(Instance: TObject; const PropName: String): TObject;
  procedure SetObjectSubProp(Instance: TObject; const PropName: String; Value: TObject);
  function GetVariantSubProp(Instance: TObject; const PropName: String): Variant;
  procedure SetVariantSubProp(Instance: TObject; const PropName: String; Value: Variant);
  function GetStrSubProp(Instance: TObject; const PropName: String): String;
  procedure SetStrSubProp(Instance: TObject; const PropName: String; Value: String);
  {$IFNDEF NO_UNICODE}
    function GetAnsiStrSubProp(Instance: TObject; const PropName: String): AnsiString;
    procedure SetAnsiStrSubProp(Instance: TObject; const PropName: String; Value: AnsiString);
  {$ENDIF}
  function GetInt64SubProp(Instance: TObject; const PropName: String): Int64;
  procedure SetInt64SubProp(Instance: TObject; const PropName: String; Value: Int64);
  function GetFloatSubProp(Instance: TObject; const PropName: String): Extended;
  procedure SetFloatSubProp(Instance: TObject; const PropName: String; Value: Extended);
  function GetOrdSubProp(Instance: TObject; const PropName: String): NativeInt;
  procedure SetOrdSubProp(Instance: TObject; const PropName: String; Value: NativeInt);
  function GetEnumSubProp(Instance: TObject; const PropName: String): String;
  procedure SetEnumSubProp(Instance: TObject; const PropName: String; Value: String);
  function GetSetSubProp(Instance: TObject; const PropName: String): String;
  procedure SetSetSubProp(Instance: TObject; const PropName: String; Value: String);
  function GetDynArraySubProp(Instance: TObject; const PropName: String): Pointer;
  procedure SetDynArraySubProp(Instance: TObject; const PropName: String; Value: Pointer);
  function GetInterfaceSubProp(Instance: TObject; const PropName: String): IInterface;
  procedure SetInterfaceSubProp(Instance: TObject; const PropName: String; Value: IInterface);
  function GetMethodSubProp(Instance: TObject; const PropName: String): TMethod;
  procedure SetMethodSubProp(Instance: TObject; const PropName: String; Value: TMethod);
  function SubPropIsType(Instance: TObject; const PropName: String; TypeKind: TTypeKind): Boolean;
  function SubPropType(Instance: TObject; const PropName: String): TTypeKind; overload;
  { Datum/Uhrzeit }
  function SystemTime: TSystemTime;
  function Year: Word;
  function Month: Word;
  function DayOfWeek: Word;
  function Day: Word;
  function Hour: Word;
  function Minute: Word;
  function Second: Word;
  function Milliseconds: Word;
  { Set-Operationen }
  function Count(Elements: TByteSet): Byte; overload;
  function Count(Elements: TCharSet): Byte; overload;
  function SetToArray(Elements: TByteSet): TByteArray; overload;
  function SetToArray(Elements: TCharSet): TCharArray; overload;
  function ArrayToSet(Elements: array of Byte): TByteSet; overload;
  function ArrayToSet(Elements: array of Char): TCharSet; overload;
  { Sonstige }
  function StringToRange(const S: String; var Range: TRange): Boolean;
  function ExprInStr(const S: String; Position: Integer): String;
  function Factional(X: Byte): Cardinal;
  function ExtractClassName(FullClassName: String; CaseSensitive: Boolean = True): String;
  function CountLines(S: String): Integer;
  function CountLine(S: String; Line: Integer): Integer;
  function Wrappable(S: String; Canvas: TCanvas; MaxWidth: Integer): Boolean;
  function WrappedTextHeight(S: String; Canvas: TCanvas; MaxWidth: Integer): Integer;
  function CharEncoding(EncodingClass: TEncoding): TCharEncoding;
  function EncodingClass(CharEncoding: TCharEncoding): TEncoding;
  function SecToTime(const Sec: Cardinal): TTime;
  function GetExecTime(Command: Pointer; Amount: Cardinal; Attempts: Cardinal = 1): Cardinal;
  function SystemLanguage: String;
  function ExtractUserName(const Owner: String): String;
  function ExtractUserDomain(const Owner: String): String;
  function RoundPos(Number: Single; Pos: Byte): Single;
  function FontSizeToHeight(Size: Integer; PpI: Integer): Integer;
  function FontHeightToSize(Height: Integer; PpI: Integer): Integer;
  function ComponentByTag(Owner: TComponent; const Tag: Integer): TComponent;
  function IntToStrMinLength(Value: Integer; MinLength: SmallInt): String;
  function MultiPos(const SubStr, Str: ShortString; Offset: Integer = 1): TIntegerArray; overload;
  function MultiPos(const SubStr, Str: String; Offset: Integer = 1): TIntegerArray; overload;
  function CharLine(Current: PAnsiChar; Text: AnsiString): Integer; {$IFNDEF NO_UNICODE} overload;
    function CharLine(Current: PWideChar; Text: UnicodeString): Integer; overload;
  {$ENDIF}
  function CharPosition(Current: PAnsiChar; Text: AnsiString): Integer; {$IFNDEF NO_UNICODE} overload;
    function CharPosition(Current: PWideChar; Text: UnicodeString): Integer; overload;
  {$ENDIF}
  function ConsistsOf(const S: String; Chars: array of Char): Boolean; overload;
  function ConsistsOf(const S: String; Chars: TCharSet): Boolean; overload;
  procedure Exchange(var X,Y); inline;
  procedure PrintText(Strings: TStrings; Font: TFont);
  procedure BFInterpret(const S: String; var P: Pointer); overload;
  procedure BFInterpret(const S: String; var P: Pointer; var ReadBuffer, WriteBuffer: TIntegerArray); overload;
  procedure ExtractChars(var Text: String; Chars: array of Char); overload;
  procedure ExtractChars(var Text: String; Chars: TCharSet); overload;
  procedure SetPrivilege(const Name: PChar; Value: Boolean); overload;
  procedure SetPrivilege(const Name: String; Value: Boolean); overload;

const
  Spaces = [#9,#10,#13,#32,#160];
  Numbers = ['0'..'9'];
  UpperCaseLetters = ['A'..'Z'];
  LowerCaseLetters = ['a'..'z'];
  Letters = UpperCaseLetters + LowerCaseLetters;
  Operators = ['+','-','*','/'];
  { WinAPI-Privilegien }
  SE_ASSIGNPRIMARYTOKEN_NAME = 'SeAssignPrimaryTokenPrivilege';
  SE_AUDIT_NAME = 'SeAuditPrivilege';
  SE_BACKUP_NAME = 'SeBackupPrivilege';
  SE_CHANGE_NOTIFY_NAME = 'SeChangeNotifyPrivilege';
  SE_CREATE_GLOBAL_NAME = 'SeCreateGlobalPrivilege';
  SE_CREATE_PAGEFILE_NAME = 'SeCreatePagefilePrivilege';
  SE_CREATE_PERMANENT_NAME = 'SeCreatePermanentPrivilege';
  SE_CREATE_SYMBOLIC_LINK_NAME = 'SeCreateSymbolicLinkPrivilege';
  SE_CREATE_TOKEN_NAME = 'SeCreateTokenPrivilege';
  SE_DEBUG_NAME = 'SeDebugPrivilege';
  SE_ENABLE_DELEGATION_NAME = 'SeEnableDelegationPrivilege';
  SE_IMPERSONATE_NAME = 'SeImpersonatePrivilege';
  SE_INC_BASE_PRIORITY_NAME = 'SeIncreaseBasePriorityPrivilege';
  SE_INCREASE_QUOTA_NAME = 'SeIncreaseQuotaPrivilege';
  SE_INC_WORKING_SET_NAME = 'SeIncreaseWorkingSetPrivilege';
  SE_LOAD_DRIVER_NAME = 'SeLoadDriverPrivilege';
  SE_LOCK_MEMORY_NAME = 'SeLockMemoryPrivilege';
  SE_MACHINE_ACCOUNT_NAME = 'SeMachineAccountPrivilege';
  SE_MANAGE_VOLUME_NAME = 'SeManageVolumePrivilege';
  SE_PROF_SINGLE_PROCESS_NAME = 'SeProfileSingleProcessPrivilege';
  SE_RELABEL_NAME = 'SeRelabelPrivilege';
  SE_REMOTE_SHUTDOWN_NAME = 'SeRemoteShutdownPrivilege';
  SE_RESTORE_NAME = 'SeRestorePrivilege';
  SE_SECURITY_NAME = 'SeSecurityPrivilege';
  SE_SHUTDOWN_NAME = 'SeShutdownPrivilege';
  SE_SYNC_AGENT_NAME = 'SeSyncAgentPrivilege';
  SE_SYSTEM_ENVIRONMENT_NAME = 'SeSystemEnvironmentPrivilege';
  SE_SYSTEM_PROFILE_NAME = 'SeSystemProfilePrivilege';
  SE_SYSTEMTIME_NAME = 'SeSystemtimePrivilege';
  SE_TAKE_OWNERSHIP_NAME = 'SeTakeOwnershipPrivilege';
  SE_TCB_NAME = 'SeTcbPrivilege';
  SE_TIME_ZONE_NAME = 'SeTimeZonePrivilege';
  SE_TRUSTED_CREDMAN_ACCESS_NAME = 'SeTrustedCredManAccessPrivilege';
  SE_UNDOCK_NAME = 'SeUndockPrivilege';
  SE_UNSOLICITED_INPUT_NAME = 'SeUnsolicitedInputPrivilege';

implementation

uses
  uFileTools;

function BoolToInt(B: Boolean): Integer; inline;
begin
  Result := -Integer(B);
end;

function IntToBool(Value: Integer): Boolean; inline;
begin
  Result := (Value <> 0);
end;

function StrIsInt(const S: String): Boolean;
var
  TryMethodBuffer: Integer;
begin
  Result := TryStrToInt(S,TryMethodBuffer);
end;

function StrIsFloat(const S: String): Boolean;
var
  TryMethodBuffer: Extended;
begin
  Result := TryStrToFloat(S,TryMethodBuffer);
end;

function StrIsBool(const S: String): Boolean;
var
  TryMethodBuffer: Boolean;
begin
  Result := TryStrToBool(S,TryMethodBuffer);
end;

function FloatIsInt(Value: Extended): Boolean;
begin
  Result := (Frac(Value) = 0);
end;

procedure ComponentSaveToFile(const FileName: String; Component: TComponent);
var
  FS: TFileStream;
begin
  FS := TFileStream.Create(FileName,fmCreate);
  try
    FS.WriteComponentRes(Component.Name,Component);
  finally
    FS.Free;
  end;
end;

procedure ComponentLoadFromFile(const FileName: String; Component: TComponent);
var
  FS: TFileStream;
begin
  FS := TFileStream.Create(FileName,fmOpenRead or fmShareDenyNone);
  try
    FS.ReadComponentRes(Component);
  finally
    FS.Free;
  end;
end;

procedure ComponentSaveToStream(var Stream: TStream; Component: TComponent);
begin
  Stream.WriteComponentRes(Component.Name,Component);
end;

procedure ComponentLoadFromStream(Stream: TStream; Component: TComponent);
begin
  Stream.ReadComponentRes(Component);
end;

function LowCase(Ch: Char): Char;
begin
  Result := CharLowerCase(Ch);
end;

function CharLowerCase(Character: Char): Char;
{ Basierend auf der Technik von SysUtils.LowerCase, nur simpler/schneller }
begin
  if Character in ['A'..'Z'] then
  begin
    Result := Char(Word(Character) or $0020);
  end else
  begin
    Result := Character;
  end;
end;

function CharUpperCase(Character: Char): Char;
{ Basierend auf der Technik von SysUtils.UpperCase, nur simpler/schneller }
begin
  if Character in ['a'..'z'] then
  begin
    Result := Char(Word(Character) xor $0020);
  end else
  begin
    Result := Character;
  end;
end;

procedure ToZero(var X: LongInt);
{$IFDEF PUREPASCAL}
begin
  if X < 0 then
  begin
    Inc(X);
  end;
  if X > 0 then
  begin
    Dec(X);
  end;
end;
{$ELSE}
asm
    CMP [EAX],0
    JE @Zer
    JL @Neg
    SUB [EAX],1
    RET
  @Neg:
    ADD [EAX],1
  @Zer:
end;
{$ENDIF}

procedure ToInf(var X: LongInt);
{$IFDEF PUREPASCAL}
begin
  if X < 0 then
  begin
    Dec(X);
  end;
  if X > 0 then
  begin
    Inc(X);
  end;
end;
{$ELSE}
asm
    CMP [EAX],0
    JE @Zer
    JL @Neg
    ADD [EAX],1
    RET
  @Neg:
    SUB [EAX],1
  @Zer:
end;
{$ENDIF}

procedure NormalizeDeg(var X: Integer);
{$IFDEF PUREPASCAL}
begin
  while X >= 360 do
  begin
    X := X - 360;
  end;
  while X < 0 do
  begin
    X := X + 360;
  end;
end;
{$ELSE}
asm
  @Cmp_Low:
    CMP [EAX],0
    JL @Low
  @Cmp_High:
    CMP [EAX],360
    JGE @High
    RET
  @Low:
    ADD [EAX],360
    JMP @Cmp_Low
  @High:
    SUB [EAX],360
    JMP @Cmp_High
end;
{$ENDIF}

procedure NormalizeDeg(var X: Single); overload;
begin
  while X >= 360 do
  begin
    X := X - 360;
  end;
  while X < 0 do
  begin
    X := X + 360;
  end;
end;

procedure NormalizeDeg(var X: Double); overload;
begin
  while X >= 360 do
  begin
    X := X - 360;
  end;
  while X < 0 do
  begin
    X := X + 360;
  end;
end;

procedure NormalizeDeg(var X: Extended); overload;
begin
  while X >= 360 do
  begin
    X := X - 360;
  end;
  while X < 0 do
  begin
    X := X + 360;
  end;
end;

function PQFormula(P,Q: Single): TSingleArray; overload;
var
  Root: Single;
begin
  Root := Sqr(P / 2) - Q;
  if Root < 0 then
  begin
    SetLength(Result,0);
  end else
  begin
    if Root = 0 then
    begin
      SetLength(Result,1);
    end else
    begin
      SetLength(Result,2);
      Result[1] := -(P / 2) - Sqrt(Root);
    end;
    Result[0] := -(P / 2) + Sqrt(Root);
  end;
end;

function PQFormula(P,Q: Double): TDoubleArray; overload;
var
  Root: Double;
begin
  Root := Sqr(P / 2) - Q;
  if Root < 0 then
  begin
    SetLength(Result,0);
  end else
  begin
    if Root = 0 then
    begin
      SetLength(Result,1);
    end else
    begin
      SetLength(Result,2);
      Result[1] := -(P / 2) - Sqrt(Root);
    end;
    Result[0] := -(P / 2) + Sqrt(Root);
  end;
end;

function PQFormula(P,Q: Extended): TExtendedArray; overload;
var
  Root: Extended;
begin
  Root := Sqr(P / 2) - Q;
  if Root < 0 then
  begin
    SetLength(Result,0);
  end else
  begin
    if Root = 0 then
    begin
      SetLength(Result,1);
    end else
    begin
      SetLength(Result,2);
      Result[1] := -(P / 2) - Sqrt(Root);
    end;
    Result[0] := -(P / 2) + Sqrt(Root);
  end;
end;

function FloatMod(X,Y: Single): Single; overload;
begin
  Result := X - Y * Trunc(X / Y);
end;

function FloatMod(X,Y: Double): Double; overload;
begin
  Result := X - Y * Trunc(X / Y);
end;

function FloatMod(X,Y: Extended): Extended; overload;
begin
  Result := X - Y * Trunc(X / Y);
end;

function IntPow(Base: ShortInt; Exponent: Byte): Int64; overload;
begin
  Result := 1;
  while Exponent > 0 do
  begin
    Result := Result * Base;
    Dec(Exponent);
  end;
end;

function IntPow(Base: SmallInt; Exponent: Byte): Int64; overload;
begin
  Result := 1;
  while Exponent > 0 do
  begin
    Result := Result * Base;
    Dec(Exponent);
  end;
end;

function IntPow(Base: Integer; Exponent: Byte): Int64; overload;
begin
  Result := 1;
  while Exponent > 0 do
  begin
    Result := Result * Base;
    Dec(Exponent);
  end;
end;

function IntPow(Base: Int64; Exponent: Byte): Int64; overload;
begin
  Result := 1;
  while Exponent > 0 do
  begin
    Result := Result * Base;
    Dec(Exponent);
  end;
end;

function GCD(A,B: Byte): Byte; overload;
begin
  if B <> 0 then
  begin
    if A > B then
    begin
      Result := B;
      B := A;
      A := Result;
    end else
    begin
      if A <> 0 then
      begin
        Result := A;
      end else
      begin
        Result := 0;
        Exit;
      end;
    end;
    while (A mod Result <> 0) or (B mod Result <> 0) do
    begin
      Dec(Result);
    end;
  end else
  begin
    Result := 0;
  end;
end;

function GCD(A,B: Word): Word; overload;
begin
  if B <> 0 then
  begin
    if A > B then
    begin
      Result := B;
      B := A;
      A := Result;
    end else
    begin
      if A <> 0 then
      begin
        Result := A;
      end else
      begin
        Result := 0;
        Exit;
      end;
    end;
    while (A mod Result <> 0) or (B mod Result <> 0) do
    begin
      Dec(Result);
    end;
  end else
  begin
    Result := 0;
  end;
end;

function GCD(A,B: Cardinal): Cardinal; overload;
begin
  if B <> 0 then
  begin
    if A > B then
    begin
      Result := B;
      B := A;
      A := Result;
    end else
    begin
      if A <> 0 then
      begin
        Result := A;
      end else
      begin
        Result := 0;
        Exit;
      end;
    end;
    while (A mod Result <> 0) or (B mod Result <> 0) do
    begin
      Dec(Result);
    end;
  end else
  begin
    Result := 0;
  end;
end;

function LCM(A,B: Byte): Word; overload;
begin
  Result := GCD(A,B);
  if Result <> 0 then
  begin
    Result := A * B div Result;
  end;
end;

function LCM(A,B: Word): Cardinal; overload;
begin
  Result := GCD(A,B);
  if Result <> 0 then
  begin
    Result := A * B div Result;
  end;
end;

function LCM(A,B: Cardinal): Cardinal; overload;
begin
  Result := GCD(A,B);
  if Result <> 0 then
  begin
    Result := A * B div Result;
  end;
end;

function SumOf(Value: Byte; Offset: Byte = 1): Word;
begin
  if Offset < Value then
  begin
    Result := (Sqr(Value) + Value) div 2 - SumOf(Offset - 1);
  end else
  begin
    Result := 0;
  end;
end;

function SumOf(Value: Word; Offset: Word = 1): Cardinal;
begin
  if Offset < Value then
  begin
    Result := (Sqr(Value) + Value) div 2 - SumOf(Offset - 1);
  end else
  begin
    Result := 0;
  end;
end;

function SumOf(Value: Cardinal; Offset: Cardinal = 1): Cardinal;
begin
  if Offset < Value then
  begin
    Result := (Sqr(Value) + Value) div 2 - SumOf(Offset - 1);
  end else
  begin
    Result := 0;
  end;
end;

function ArrayAdd(A,B: array of ShortInt): TShortIntArray;
begin

end;

function ArrayAdd(A,B: array of SmallInt): TSmallIntArray;
begin

end;

function ArrayAdd(A,B: array of Integer): TIntegerArray;
begin

end;

function ArrayAdd(A,B: array of Int64): TInt64Array;
begin

end;

function ArrayAdd(A,B: array of Byte): TByteArray;
begin

end;

function ArrayAdd(A,B: array of Word): TWordArray;
begin

end;

function ArrayAdd(A,B: array of Cardinal): TCardinalArray;
begin

end;

function ArrayAdd(A,B: array of Single): TSingleArray;
begin

end;

function ArrayAdd(A,B: array of Double): TDoubleArray;
begin

end;

function ArrayAdd(A,B: array of Extended): TExtendedArray;
begin

end;

procedure BubbleSort(var Elements: array of String); overload;
var
  Changed: Boolean;
  Index: Integer;
  Buffer: String;
begin
  Changed := True;
  while Changed = True do
  begin
    Changed := False;
    for Index := Low(Elements) to High(Elements) - 1 do
    begin
      if AnsiCompareText(Elements[Index],Elements[Index + 1]) > 0 then
      begin
        Buffer := Elements[Index];
        Elements[Index] := Elements[Index + 1];
        Elements[Index + 1] := Buffer;
        if Changed = False then
        begin
          Changed := True;
        end;
      end;
    end;
  end;
end;

procedure BubbleSort(var Elements: array of ShortInt); overload;
var
  Changed: Boolean;
  Index: Integer;
  Buffer: ShortInt;
begin
  Changed := True;
  while Changed = True do
  begin
    Changed := False;
    for Index := Low(Elements) to High(Elements) - 1 do
    begin
      if Elements[Index] > Elements[Index + 1] then
      begin
        Buffer := Elements[Index];
        Elements[Index] := Elements[Index + 1];
        Elements[Index + 1] := Buffer;
        if Changed = False then
        begin
          Changed := True;
        end;
      end;
    end;
  end;
end;

procedure BubbleSort(var Elements: array of SmallInt); overload;
var
  Changed: Boolean;
  Index: Integer;
  Buffer: SmallInt;
begin
  Changed := True;
  while Changed = True do
  begin
    Changed := False;
    for Index := Low(Elements) to High(Elements) - 1 do
    begin
      if Elements[Index] > Elements[Index + 1] then
      begin
        Buffer := Elements[Index];
        Elements[Index] := Elements[Index + 1];
        Elements[Index + 1] := Buffer;
        if Changed = False then
        begin
          Changed := True;
        end;
      end;
    end;
  end;
end;

procedure BubbleSort(var Elements: array of Integer); overload;
var
  Changed: Boolean;
  Index: Integer;
  Buffer: Integer;
begin
  Changed := True;
  while Changed = True do
  begin
    Changed := False;
    for Index := Low(Elements) to High(Elements) - 1 do
    begin
      if Elements[Index] > Elements[Index + 1] then
      begin
        Buffer := Elements[Index];
        Elements[Index] := Elements[Index + 1];
        Elements[Index + 1] := Buffer;
        if Changed = False then
        begin
          Changed := True;
        end;
      end;
    end;
  end;
end;

procedure BubbleSort(var Elements: array of Int64); overload;
var
  Changed: Boolean;
  Index: Integer;
  Buffer: Int64;
begin
  Changed := True;
  while Changed = True do
  begin
    Changed := False;
    for Index := Low(Elements) to High(Elements) - 1 do
    begin
      if Elements[Index] > Elements[Index + 1] then
      begin
        Buffer := Elements[Index];
        Elements[Index] := Elements[Index + 1];
        Elements[Index + 1] := Buffer;
        if Changed = False then
        begin
          Changed := True;
        end;
      end;
    end;
  end;
end;

procedure BubbleSort(var Elements: array of Byte); overload;
var
  Changed: Boolean;
  Index: Integer;
  Buffer: Byte;
begin
  Changed := True;
  while Changed = True do
  begin
    Changed := False;
    for Index := Low(Elements) to High(Elements) - 1 do
    begin
      if Elements[Index] > Elements[Index + 1] then
      begin
        Buffer := Elements[Index];
        Elements[Index] := Elements[Index + 1];
        Elements[Index + 1] := Buffer;
        if Changed = False then
        begin
          Changed := True;
        end;
      end;
    end;
  end;
end;

procedure BubbleSort(var Elements: array of Word); overload;
var
  Changed: Boolean;
  Index: Integer;
  Buffer: Word;
begin
  Changed := True;
  while Changed = True do
  begin
    Changed := False;
    for Index := Low(Elements) to High(Elements) - 1 do
    begin
      if Elements[Index] > Elements[Index + 1] then
      begin
        Buffer := Elements[Index];
        Elements[Index] := Elements[Index + 1];
        Elements[Index + 1] := Buffer;
        if Changed = False then
        begin
          Changed := True;
        end;
      end;
    end;
  end;
end;

procedure BubbleSort(var Elements: array of Cardinal); overload;
var
  Changed: Boolean;
  Index: Integer;
  Buffer: Cardinal;
begin
  Changed := True;
  while Changed = True do
  begin
    Changed := False;
    for Index := Low(Elements) to High(Elements) - 1 do
    begin
      if Elements[Index] > Elements[Index + 1] then
      begin
        Buffer := Elements[Index];
        Elements[Index] := Elements[Index + 1];
        Elements[Index + 1] := Buffer;
        if Changed = False then
        begin
          Changed := True;
        end;
      end;
    end;
  end;
end;

procedure BubbleSort(var Elements: array of Single); overload;
var
  Changed: Boolean;
  Index: Integer;
  Buffer: Single;
begin
  Changed := True;
  while Changed = True do
  begin
    Changed := False;
    for Index := Low(Elements) to High(Elements) - 1 do
    begin
      if Elements[Index] > Elements[Index + 1] then
      begin
        Buffer := Elements[Index];
        Elements[Index] := Elements[Index + 1];
        Elements[Index + 1] := Buffer;
        if Changed = False then
        begin
          Changed := True;
        end;
      end;
    end;
  end;
end;

procedure BubbleSort(var Elements: array of Double); overload;
var
  Changed: Boolean;
  Index: Integer;
  Buffer: Double;
begin
  Changed := True;
  while Changed = True do
  begin
    Changed := False;
    for Index := Low(Elements) to High(Elements) - 1 do
    begin
      if Elements[Index] > Elements[Index + 1] then
      begin
        Buffer := Elements[Index];
        Elements[Index] := Elements[Index + 1];
        Elements[Index + 1] := Buffer;
        if Changed = False then
        begin
          Changed := True;
        end;
      end;
    end;
  end;
end;

procedure BubbleSort(var Elements: array of Extended); overload;
var
  Changed: Boolean;
  Index: Integer;
  Buffer: Extended;
begin
  Changed := True;
  while Changed = True do
  begin
    Changed := False;
    for Index := Low(Elements) to High(Elements) - 1 do
    begin
      if Elements[Index] > Elements[Index + 1] then
      begin
        Buffer := Elements[Index];
        Elements[Index] := Elements[Index + 1];
        Elements[Index + 1] := Buffer;
        if Changed = False then
        begin
          Changed := True;
        end;
      end;
    end;
  end;
end;

function GetSubPropInfo(Instance: TObject; const PropName: String; AKinds: TTypeKinds = []): PPropInfo;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetPropInfo(Instance,Trim(PropName),AKinds);
  end else
  begin
    Result := GetSubPropInfo(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),AKinds);
  end;
end;

function GetObjectSubProp(Instance: TObject; const PropName: String): TObject;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetObjectProp(Instance,Trim(PropName));
  end else
  begin
    Result := GetObjectSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

procedure SetObjectSubProp(Instance: TObject; const PropName: String; Value: TObject);
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    SetObjectProp(Instance,Trim(PropName),Value);
  end else
  begin
    SetObjectSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),Value);
  end;
end;

function GetVariantSubProp(Instance: TObject; const PropName: String): Variant;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetVariantProp(Instance,Trim(PropName));
  end else
  begin
    Result := GetVariantSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

procedure SetVariantSubProp(Instance: TObject; const PropName: String; Value: Variant);
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    SetVariantProp(Instance,Trim(PropName),Value);
  end else
  begin
    SetVariantSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),Value);
  end;
end;

function GetStrSubProp(Instance: TObject; const PropName: String): String;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetStrProp(Instance,Trim(PropName));
  end else
  begin
    Result := GetStrSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

procedure SetStrSubProp(Instance: TObject; const PropName: String; Value: String);
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    SetStrProp(Instance,Trim(PropName),Value);
  end else
  begin
    SetStrSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),Value);
  end;
end;

{$IFNDEF NO_UNICODE}
function GetAnsiStrSubProp(Instance: TObject; const PropName: String): AnsiString;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetAnsiStrProp(Instance,Trim(PropName));
  end else
  begin
    Result := GetAnsiStrSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

procedure SetAnsiStrSubProp(Instance: TObject; const PropName: String; Value: AnsiString);
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    SetAnsiStrProp(Instance,Trim(PropName),Value);
  end else
  begin
    SetAnsiStrSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),Value);
  end;
end;
{$ENDIF}

function GetInt64SubProp(Instance: TObject; const PropName: String): Int64;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetInt64Prop(Instance,Trim(PropName));
  end else
  begin
    Result := GetInt64SubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

procedure SetInt64SubProp(Instance: TObject; const PropName: String; Value: Int64);
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    SetInt64Prop(Instance,Trim(PropName),Value);
  end else
  begin
    SetInt64SubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),Value);
  end;
end;

function GetFloatSubProp(Instance: TObject; const PropName: String): Extended;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetFloatProp(Instance,Trim(PropName));
  end else
  begin
    Result := GetFloatSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

procedure SetFloatSubProp(Instance: TObject; const PropName: String; Value: Extended);
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    SetFloatProp(Instance,Trim(PropName),Value);
  end else
  begin
    SetFloatSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),Value);
  end;
end;

function GetOrdSubProp(Instance: TObject; const PropName: String): NativeInt;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetOrdProp(Instance,Trim(PropName));
  end else
  begin
    Result := GetOrdSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

procedure SetOrdSubProp(Instance: TObject; const PropName: String; Value: NativeInt);
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    SetOrdProp(Instance,Trim(PropName),Value);
  end else
  begin
    SetOrdSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),Value);
  end;
end;

function GetEnumSubProp(Instance: TObject; const PropName: String): String;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetEnumProp(Instance,Trim(PropName));
  end else
  begin
    Result := GetEnumSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

procedure SetEnumSubProp(Instance: TObject; const PropName: String; Value: String);
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    SetEnumProp(Instance,Trim(PropName),Value);
  end else
  begin
    SetEnumSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),Value);
  end;
end;

function GetSetSubProp(Instance: TObject; const PropName: String): String;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetSetProp(Instance,Trim(PropName));
  end else
  begin
    Result := GetSetSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

procedure SetSetSubProp(Instance: TObject; const PropName: String; Value: String);
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    SetSetProp(Instance,Trim(PropName),Value);
  end else
  begin
    SetSetSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),Value);
  end;
end;

function GetDynArraySubProp(Instance: TObject; const PropName: String): Pointer;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetDynArrayProp(Instance,Trim(PropName));
  end else
  begin
    Result := GetDynArraySubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

procedure SetDynArraySubProp(Instance: TObject; const PropName: String; Value: Pointer);
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    SetDynArrayProp(Instance,Trim(PropName),Value);
  end else
  begin
    SetDynArraySubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),Value);
  end;
end;

function GetInterfaceSubProp(Instance: TObject; const PropName: String): IInterface;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetInterfaceProp(Instance,Trim(PropName));
  end else
  begin
    Result := GetInterfaceSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

procedure SetInterfaceSubProp(Instance: TObject; const PropName: String; Value: IInterface);
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    SetInterfaceProp(Instance,Trim(PropName),Value);
  end else
  begin
    SetInterfaceSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),Value);
  end;
end;

function GetMethodSubProp(Instance: TObject; const PropName: String): TMethod;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := GetMethodProp(Instance,Trim(PropName));
  end else
  begin
    Result := GetMethodSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

procedure SetMethodSubProp(Instance: TObject; const PropName: String; Value: TMethod);
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    SetMethodProp(Instance,Trim(PropName),Value);
  end else
  begin
    SetMethodSubProp(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),Value);
  end;
end;

function SubPropIsType(Instance: TObject; const PropName: String; TypeKind: TTypeKind): Boolean;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := PropIsType(Instance,Trim(PropName),TypeKind);
  end else
  begin
    Result := SubPropIsType(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2),TypeKind);
  end;
end;

function SubPropType(Instance: TObject; const PropName: String): TTypeKind;
var
  DotPos: Integer;
begin
  DotPos := Pos(DotSep,PropName);
  if DotPos = 0 then
  begin
    Result := PropType(Instance,Trim(PropName));
  end else
  begin
    Result := SubPropType(GetObjectProp(Instance,Trim(Copy(PropName,1,DotPos - 1))),Copy(PropName,DotPos + 1,Length(PropName) - 2));
  end;
end;

function SystemTime: TSystemTime;
begin
  GetLocalTime(Result);
end;

function Year: Word;
begin
  Result := SystemTime.wYear;
end;

function Month: Word;
begin
  Result := SystemTime.wMonth;
end;

function DayOfWeek: Word;
begin
  Result := SystemTime.wDayOfWeek;
end;

function Day: Word;
begin
  Result := SystemTime.wDay;
end;

function Hour: Word;
begin
  Result := SystemTime.wHour;
end;

function Minute: Word;
begin
  Result := SystemTime.wMinute;
end;

function Second: Word;
begin
  Result := SystemTime.wSecond;
end;

function Milliseconds: Word;
begin
  Result := SystemTime.wMilliseconds;
end;

function Count(Elements: TByteSet): Byte;
var
  Current: Byte;
begin
  Result := 0;
  for Current in Elements do
  begin
    Inc(Result);
  end;
end;

function Count(Elements: TCharSet): Byte;
var
  Current: Char;
begin
  Result := 0;
  for Current in Elements do
  begin
    Inc(Result);
  end;
end;

function SetToArray(Elements: TByteSet): TByteArray;
var
  Current: Byte;
  Index: Integer;
begin
  SetLength(Result,Count(Elements));
  Index := 0;
  for Current in Elements do
  begin
    Result[Index] := Current;
    Inc(Index);
  end;
end;

function SetToArray(Elements: TCharSet): TCharArray;
var
  Current: Char;
  Index: Integer;
begin
  SetLength(Result,Count(Elements));
  Index := 0;
  for Current in Elements do
  begin
    Result[Index] := Current;
    Inc(Index);
  end;
end;

function ArrayToSet(Elements: array of Byte): TByteSet;
var
  Index: Integer;
begin
  Result := [];
  for Index := 0 to Length(Elements) - 1 do
  begin
    Result := Result + [Elements[Index]];
  end;
end;

function ArrayToSet(Elements: array of Char): TCharSet;
var
  Index: Integer;
begin
  Result := [];
  for Index := 0 to Length(Elements) - 1 do
  begin
    Result := Result + [Elements[Index]];
  end;
end;

function StringToRange(const S: String; var Range: TRange): Boolean;
var
  Current: PChar;
  Block: String;
  Temp: TRange;
  Position: (posOffset,posDots,posTarget,posFinish);
begin
  {if Range = nil then
  begin
    Range := TRange.Create;
  end;
  Result := False;
  if Length(S) = 0 then
  begin
    Exit;
  end;
  Current := PChar(S);
  Position := posOffset;
  Temp := TRange.Create;
  try
    while Current^ <> #0 do
    begin
      if (Position^ = posFinish) and (not (Current^ in Spaces)) then
      begin
        Result := False;
        Break;
      end;
      case Current^ of
      Spaces: if Position in [posOffset,posTarget] then
              begin
                Inc(Position);
              end;
      '0'..'9': Block := Block + Current^;
      '+','-': if Length(Block) = 0 then
               begin
                 Block := Block + Current^;
               end else
               begin
                 Result := False;
                 Break;
               end;
      '.': if (Current + 1)^ = '.' then
           begin

             Block := '';
             Inc(Current);
           end else
           begin
             Result := False;
             Break;
           end;
      end;
      Inc(Current);
    end;
  finally
    Temp.Free;
  end;
  if Result = True then
  begin
    Range.Offset := Temp.Offset;
    Range.Target := Temp.Target;
    Range.Step := Temp.Step;
  end;        }
end;

function ExprInStr(const S: String; Position: Integer): String;
var
  Current: PChar;
begin
  if (Position < 1) or (Position > Length(S)) then
  begin
    raise EStringCharAccess.Create('Access to String char at position ' + IntToStr(Position) + ' not possible');
  end;
  Current := @S[Position];
  Result := '';
  while (Current^ in Letters) and (Current >= @S[1]) do
  begin
    Result := Current^ + Result;
    Dec(Current);
  end;
  Current := @S[Position + 1];
  while Current^ in Letters do
  begin
    Result := Result + Current^;
    Inc(Current);
  end;
end;

function Factional(X: Byte): Cardinal;
{$IFDEF PUREPASCAL}
var
  Index: Integer;
begin
  Result := 1;
  for Index := 2 to X do
  begin
    Result := Result * Index;
  end;
end;
{$ELSE}
asm
    MOV ECX,EAX
    MOV EAX,1
    JMP @Check
  @Multiply:
    IMUL EAX,ECX
    DEC ECX
  @Check:
    CMP ECX,1
    JNE @Multiply
end;
{$ENDIF}

function ExtractClassName(FullClassName: String; CaseSensitive: Boolean = True): String;
begin
  if (Length(FullClassName) <> 0) and ((FullClassName[1] = 'T') or ((CaseSensitive = False) and (FullClassName[1] = 't'))) and ((FullClassName[2] in UppercaseLetters) or (CaseSensitive = False)) then
  begin
    Result := Copy(FullClassName,2,Length(FullClassName) - 1);
  end else
  begin
    Result := FullClassName;
  end;
end;

function CountLines(S: String): Integer;
var
  Current: PChar;
begin
  if Length(S) = 0 then
  begin
    Result := 0;
    Exit;
  end;
  Result := 1;
  Current := PChar(S);
  while Current^ <> #0 do
  begin
    if Current^ = #13 then
    begin
      if (Current + 1)^ = #10 then
      begin
        Inc(Result);
        Inc(Current);
      end;
    end;
    Inc(Current);
  end;
end;

function CountLine(S: String; Line: Integer): Integer;
var
  Current: PChar;
begin
  if Line < 0 then
  begin
    Result := -1;
    Exit;
  end;
  Result := 0;
  Current := PChar(S);
  while Current^ <> #0 do
  begin
    if Current^ = #13 then
    begin
      if (Current + 1)^ = #10 then
      begin
        if Line = 0 then
        begin
          Exit;
        end;
        Dec(Line);
        Inc(Current);
        Continue;
      end;
    end;
    if Line = 0 then
    begin
      Inc(Result);
    end;
    Inc(Current);
  end;
  if Line <> 0 then
  begin
    Result := -1;
  end;
end;

function Wrappable(S: String; Canvas: TCanvas; MaxWidth: Integer): Boolean; overload;
begin
  Result := (Canvas.TextWidth(S) <= MaxWidth);
end;

function WrappedTextHeight(S: String; Canvas: TCanvas; MaxWidth: Integer): Integer;
var
  Current: PChar;
  Line: String;
begin
  Result := 0;
  if (Length(S) = 0) or (MaxWidth = 0) then
  begin
    Exit;
  end;
  Line := '';
  Current := PChar(S);
  while Current^ <> #0 do
  begin
    if ((Current^ = #13) and ((Current + 1)^ = #10)) or (Canvas.TextWidth(Line + Current^) > MaxWidth) then
    begin
      Inc(Result,Canvas.TextHeight(Line));
      Line := Current^;
      if (Current + 1)^ = #10 then
      begin
        Inc(Current);
      end;
    end else
    begin
      Line := Line + Current^;
    end;
    Inc(Current);
  end;
  Inc(Result,Canvas.TextHeight(Line));
end;

function CharEncoding(EncodingClass: TEncoding): TCharEncoding;
begin
  if EncodingClass = TEncoding.ANSI then
  begin
    Result := ceANSI;
  end else
  begin
    if EncodingClass = TEncoding.ASCII then
    begin
      Result := ceASCII;
    end else
    begin
      if EncodingClass = TEncoding.BigEndianUnicode then
      begin
        Result := ceBigEndianUnicode;
      end else
      begin
        if EncodingClass = TEncoding.Unicode then
        begin
          Result := ceUnicode;
        end else
        begin
          if EncodingClass = TEncoding.UTF7 then
          begin
            Result := ceUTF7;
          end else
          begin
            if EncodingClass = TEncoding.UTF8 then
            begin
              Result := ceUTF8;
            end else
            begin
              raise EConvertError.Create('Invalid encoding type');
            end;
          end;
        end;
      end;
    end;
  end;
end;

function EncodingClass(CharEncoding: TCharEncoding): TEncoding;
begin
  case CharEncoding of
    ceANSI: Result := TEncoding.ANSI;
    ceASCII: Result := TEncoding.ASCII;
    ceBigEndianUnicode: Result := TEncoding.BigEndianUnicode;
    ceUnicode: Result := TEncoding.Unicode;
    ceUTF7: Result := TEncoding.UTF7;
    ceUTF8: Result := TEncoding.UTF8;
  end;
end;

function SecToTime(const Sec: Cardinal): TTime;
var
   Hrs, Mins: Word;
begin
  Hrs := Sec div 3600;
  Mins := Sec div 60 - Hrs * 60;
  Result := StrToTime(IntToStr(Hrs) + ':' + IntToStr(Mins) + ':' + IntToStr(Sec - (Hrs * 3600 + Mins * 60)));
end;

function GetExecTime(Command: Pointer; Amount: Cardinal; Attempts: Cardinal = 1): Cardinal;
var
  Index_Amount: Cardinal;
  Index_Attempts: Cardinal;
  Tick_Start: Cardinal;
  Tick_Finish: Cardinal;
begin
  Result := 0;
  if (Assigned(Command) = False) or (Amount = 0) or (Attempts = 0) then
  begin
    Exit;
  end;
  try
    for Index_Attempts := 1 to Attempts do
    begin
      for Index_Amount := 1 to Amount do
      begin
        Tick_Start := GetTickCount;
        TProcedure(Command);
        Tick_Finish := GetTickCount;
        Inc(Result,Tick_Finish - Tick_Start);
      end;
    end;
  finally
    Result := Result div Index_Attempts;
  end;
end;

function SystemLanguage: String;
begin
  Result := Languages.NameFromLocaleID[Languages.UserDefaultLocale];
end;

function ArrayPos(const AValue; const AArray: array of const): Integer; overload;
{ Da diese Methode anonyme Parameter �bergeben bekommt (bzw. die Parameter
  anonym �bergeben werden), entsteht aufgrund der Typen�berpr�fung f�r jedes
  Element von AArray eine erhebliche Performanzeinbu�e.
  Diese Methode sollte deswegen nur in Ausnamef�llen verwendet werden.
  Sofern m�glich, sollte alternativ auch die Funktion ArrayPos(Variant, array of
  Vairant) verwendet werden. }
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    case AArray[Index].VType of
      vtInteger: if AArray[Index].VInteger = Integer(AValue) then
                 begin
                   Result := Index;
                   Exit;
                 end;
      vtBoolean: if AArray[Index].VBoolean = Boolean(AValue) then
                 begin
                   Result := Index;
                   Exit;
                 end;
      vtChar:  if AArray[Index].VChar = AnsiChar(AValue) then
               begin
                 Result := Index;
                 Exit;
               end;
      vtExtended: if AArray[Index].VExtended = PExtended(AValue) then
                  begin
                    Result := Index;
                    Exit;
                  end;
      vtString: if AArray[Index].VString = PShortString(AValue) then
                begin
                  Result := Index;
                  Exit;
                end;
      vtPointer: if AArray[Index].VPointer = Pointer(AValue) then
                 begin
                   Result := Index;
                   Exit;
                 end;
      vtPChar: if AArray[Index].VPChar = PAnsiChar(AValue) then
               begin
                 Result := Index;
                 Exit;
               end;
      vtObject: if AArray[Index].VObject = TObject(AValue) then
                begin
                  Result := Index;
                  Exit;
                end;
      vtClass: if AArray[Index].VClass = TClass(AValue) then
               begin
                 Result := Index;
                 Exit;
               end;
      {$IFNDEF NO_UNICODE}
        vtWideChar: if AArray[Index].VWideChar = Char(AValue) then
                    begin
                      Result := Index;
                      Exit;
                    end;
        vtPWideChar: if AArray[Index].VWideChar = PChar(AValue) then
                     begin
                       Result := Index;
                       Exit;
                     end;
        vtUnicodeString: if AArray[Index].VUnicodeString = Pointer(AValue) then
                         begin
                           Result := Index;
                           Exit;
                         end;
      {$ENDIF}
      vtAnsiString: if AArray[Index].VAnsiString = Pointer(AValue) then
                    begin
                      Result := Index;
                      Exit;
                    end;
      vtCurrency: if AArray[Index].VCurrency = PCurrency(AValue) then
                  begin
                    Result := Index;
                    Exit;
                  end;
      vtVariant: if AArray[Index].VVariant = PVariant(AValue) then
                 begin
                   Result := Index;
                   Exit;
                 end;
      vtInterface: if AArray[Index].VInterface = Pointer(AValue) then
                   begin
                     Result := Index;
                     Exit;
                   end;

      vtWideString: if AArray[Index].VWideString = Pointer(AValue) then
                    begin
                      Result := Index;
                      Exit;
                    end;
      vtInt64: if AArray[Index].VInt64 = PInt64(AValue) then
               begin
                 Result := Index;
                 Exit;
               end;
    end;
  end;
end;

function ArrayPos(const AValue: Variant; const AArray: array of Variant): Integer; overload;
{ Da diese Funktion als Parametertyp Variants verwendet, wird von der Verwendung
  bei komplexeren Abl�ufen aufgrund der geringen Performance varianter Typen
  ausdr�cklich abgeraten! }
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: Pointer; const AArray: array of Pointer): Integer;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: Char; const AArray: array of Char): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: ShortString; const AArray: array of ShortString): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: String; const AArray: array of String): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: ShortInt; const AArray: array of ShortInt): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: SmallInt; const AArray: array of SmallInt): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: LongInt; const AArray: array of LongInt): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: Int64; const AArray: array of Int64): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: Byte; const AArray: array of Byte): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: Word; const AArray: array of Word): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: Cardinal; const AArray: array of Cardinal): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: UInt64; const AArray: array of UInt64): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: Single; const AArray: array of Single): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: Double; const AArray: array of Double): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: Real; const AArray: array of Real): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPos(const AValue: Extended; const AArray: array of Extended): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPosRef(const AValue: Pointer; const AArray: array of TReferenceData): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index].Value = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPosRef(const AValue: String; const AArray: array of TStringReferenceData; IgnoreCase: Boolean = False): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if (AArray[Index].Value = AValue) or ((IgnoreCase = True) and (LowerCase(AArray[Index].Value) = LowerCase(AValue))) then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPosRef(const AValue: Integer; const AArray: array of TIntegerReferenceData): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index].Value = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPosRef(const AValue: Extended; const AArray: array of TFloatReferenceData): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index].Value = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPosRef(const AValue: Pointer; const AArray: array of TRefDataArrayReferenceData): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index].Value = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPosRef(const AValue: String; const AArray: array of TStringRefDataArrayReferenceData; IgnoreCase: Boolean = False): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if (AArray[Index].Value = AValue) or ((IgnoreCase = True) and (LowerCase(AArray[Index].Value) = LowerCase(AValue))) then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPosRef(const AValue: Integer; const AArray: array of TIntegerRefDataArrayReferenceData): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index].Value = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPosRef(const AValue: Extended; const AArray: array of TFloatRefDataArrayReferenceData): Integer; overload;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index].Value = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPosType(AValue: TClass; AArray: array of TClass): Integer;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] = AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

function ArrayPosType(AValue: TClass; AArray: array of TObject): Integer;
var
  Index: Integer;
begin
  Result := Low(AArray) - 1;
  for Index := Low(AArray) to High(AArray) do
  begin
    if AArray[Index] is AValue then
    begin
      Result := Index;
      Exit;
    end;
  end;
end;

procedure ArrayDelete(var AArray: TVariantArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TCharArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TShortStringArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TStringArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TShortIntArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TSmallIntArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TIntegerArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TInt64Array; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TByteArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TWordArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TCardinalArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TUInt64Array; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TSingleArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TDoubleArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

procedure ArrayDelete(var AArray: TExtendedArray; Index: Integer; Count: Integer);
begin
  if Count <> 0 then
  begin
    Move(AArray[Index + Count],AArray[Index],SizeOf(AArray[0]) * (Length(AArray) - Index - Count));
    SetLength(AArray,Length(AArray) - Count);
  end;
end;

function ExtractUserName(const Owner: String): String;
var
  Index: Integer;
begin
  for Index := 1 to Length(Owner) do
  begin
    if Owner[Index] = '@' then
    begin
      Break;
    end else
    begin
      Result := Result + Owner[Index];
    end;
  end;
end;

function ExtractUserDomain(const Owner: String): String;
var
  Index: Integer;
begin
  for Index := Length(Owner) downto 0 do
  begin
    if Owner[Index] = '@' then
    begin
      Break;
    end else
    begin
      Result := Result + Owner[Index];
    end;
  end;
end;

function RoundPos(Number: Single; Pos: Byte): Single;
var
  Factor: Single;
begin
  Factor := IntPower(10,Pos);
  Result := Round(Number * Factor) / Factor;
end;

function FontSizeToHeight(Size: Integer; PpI: Integer): Integer;
begin
  Result := - Size * PpI div 72;
end;

function FontHeightToSize(Height: Integer; PpI: Integer): Integer;
begin
  Result := - Height * 72 div PpI;
end;

function ComponentByTag(Owner: TComponent; const Tag: Integer): TComponent;
var
  Index: Integer;
begin
  for Index := 0 to Owner.ComponentCount - 1 do
  begin
    if Owner.Components[Index].Tag = Tag then
    begin
      Result := Owner.Components[Index];
      Exit;
    end;
  end;
  Result := nil;
end;

function IntToStrMinLength(Value: Integer; MinLength: SmallInt): String;
begin
  Result := IntToStr(Value);
  while Length(Result) < MinLength do
  begin
    Result := '0' + Result;
  end;
end;

function MultiPos(const SubStr, Str: ShortString; Offset: Integer = 1): TIntegerArray;
begin

end;

function MultiPos(const SubStr, Str: String; Offset: Integer = 1): TIntegerArray;
var
  Temp: PChar;
  Position: Integer;
  Further: TIntegerArray;
begin
  SetLength(Result,0);
  if (Offset < 1) or (Offset - 1 > (Length(Str) - Length(SubStr))) then
  begin
    Exit;
  end;
  Temp := @Str[OffSet];
  Position := Pos(SubStr,String(Temp));
  if Position <> 0 then
  begin
    SetLength(Result,1);
    Result[0] := Position;
    Further := MultiPos(SubStr,Str,Offset + Position + Length(SubStr) - 1);
    if Length(Further) <> 0 then
    begin
      SetLength(Result,1 + Length(Further));
      Move(Further[0],Result[1],SizeOf(Further) * Length(Further));
    end;
  end;
end;

function CharLine(Current: PAnsiChar; Text: AnsiString): Integer;
var
  Index: PAnsiChar;
  InLineBreak: PAnsiChar;
  Position: Integer;
begin
  Position := Pos(AnsiString(Current),Text);
  if Position = 0 then
  begin
    if Current^ = #0 then
    begin
      Result := Length(Text) + 1;
    end else
    begin
      Result := 0;
    end;
    Exit;
  end;
  Result := 0;
  SetLength(Text,Position - 1);
  InLineBreak := @sLineBreak[1];
  Index := PAnsiChar(Text);
  while Index^ <> #0 do
  begin
    if Index^ = InLineBreak^ then
    begin
      Inc(InLineBreak);
      if InLineBreak^ = #0 then
      begin
        Inc(Result);
        InLineBreak := @sLineBreak[1];
      end;
    end else
    begin
      InLineBreak := @sLineBreak[1];
    end;
    Inc(Index);
  end;
end;

{$IFNDEF NO_UNICODE}
function CharLine(Current: PWideChar; Text: UnicodeString): Integer;
var
  Index: PWideChar;
  InLineBreak: PWideChar;
  Position: Integer;
  UnicodeLineBreak: UnicodeString;
begin
  Position := Pos(UnicodeString(Current),Text);
  if Position = 0 then
  begin
    if Current^ = #0 then
    begin
      Result := Length(Text) + 1;
    end else
    begin
      Result := 0;
    end;
    Exit;
  end;
  UnicodeLineBreak := UnicodeString(sLineBreak);
  Result := 0;
  SetLength(Text,Position - 1);
  InLineBreak := @UnicodeLineBreak[1];
  Index := PChar(Text);
  while Index^ <> #0 do
  begin
    if Index^ = InLineBreak^ then
    begin
      Inc(InLineBreak);
      if InLineBreak^ = #0 then
      begin
        Inc(Result);
        InLineBreak := @UnicodeLineBreak[1];
      end;
    end else
    begin
      InLineBreak := @UnicodeLineBreak[1];
    end;
    Inc(Index);
  end;
end;
{$ENDIF}

function CharPosition(Current: PAnsiChar; Text: AnsiString): Integer;
var
  Index: PAnsiChar;
  InLineBreak: PAnsiChar;
  Position: Integer;
  Line: Integer;
begin
  Position := Pos(AnsiString(Current),Text);
  if Position = 0 then
  begin
    if Current^ = #0 then
    begin
      Result := Length(Text) + 1;
    end else
    begin
      Result := 0;
    end;
    Exit;
  end;
  Result := 1;
  SetLength(Text,Position - 1);
  InLineBreak := @sLineBreak[1];
  Index := PAnsiChar(Text);
  while Index^ <> #0 do
  begin
    if Index^ = InLineBreak^ then
    begin
      Inc(InLineBreak);
      if InLineBreak^ = #0 then
      begin
        Result := 1;
        Inc(Line);
        InLineBreak := @sLineBreak[1];
      end;
    end else
    begin
      InLineBreak := @sLineBreak[1];
      Inc(Result);
    end;
    Inc(Index);
  end;
end;

{$IFNDEF NO_UNICODE}
function CharPosition(Current: PWideChar; Text: UnicodeString): Integer;
var
  Index: PWideChar;
  InLineBreak: PWideChar;
  Position: Integer;
  UnicodeLineBreak: UnicodeString;
  Line: Integer;
begin
  Position := Pos(UnicodeString(Current),Text);
  if Position = 0 then
  begin
    if Current^ = #0 then
    begin
      Result := Length(Text) + 1;
    end else
    begin
      Result := 0;
    end;
    Exit;
  end;
  UnicodeLineBreak := UnicodeString(sLineBreak);
  Result := 1;
  SetLength(Text,Position - 1);
  InLineBreak := @UnicodeLineBreak[1];
  Index := PChar(Text);
  while Index^ <> #0 do
  begin
    if Index^ = InLineBreak^ then
    begin
      Inc(InLineBreak);
      if InLineBreak^ = #0 then
      begin
        Result := 1;
        Inc(Line);
        InLineBreak := @UnicodeLineBreak[1];
      end;
    end else
    begin
      InLineBreak := @UnicodeLineBreak[1];
      Inc(Result);
    end;
    Inc(Index);
  end;
end;
{$ENDIF}

function ConsistsOf(const S: String; Chars: array of Char): Boolean; overload;
var
  Current: PChar;
begin
  Current := PChar(S);
  while Current^ <> #0 do
  begin
    if ArrayPos(Current^,Chars) = -1 then
    begin
      Result := False;
      Exit;
    end;
    Inc(Current);
  end;
  Result := True;
end;

function ConsistsOf(const S: String; Chars: TCharSet): Boolean; overload;
var
  Current: PChar;
begin
  Current := PChar(S);
  while Current^ <> #0 do
  begin
    if not (Current^ in Chars) then
    begin
      Result := False;
      Exit;
    end;
    Inc(Current);
  end;
  Result := True;
end;

procedure Exchange(var X,Y); inline;
var
  Buffer: Pointer;
begin
  Buffer := Pointer(X);
  Pointer(X) := Pointer(Y);
  Pointer(Y) := Buffer;
end;

procedure PrintText(Strings: TStrings; Font: TFont);
var
  Index: Integer;
  PrintText: TextFile;
begin
  AssignPrn(PrintText);
  Rewrite(PrintText);
  try
    if Font = nil then
    begin
      Printer.Canvas.Font := Font;
    end;
    for Index := 0 to Strings.Count - 1 do
    begin
      Writeln(PrintText, Strings.Strings[Index]);
    end;
  finally
    CloseFile(PrintText);
  end;
end;

procedure BFInterpret(const S: String; var P: Pointer);
{ Liest einen BF-Quelltext "S" ein und ver�ndert den Zeiger "P" entsprechend.
  Ung�ltige Zeichen l�sen eine Fehlermeldung aus. Eingaben werden aus der
  aktuellen Datei oder der Konsole eingelesen und Ausgaben dort ausgegeben. }
var
  Current: PChar;
begin
  if P <> nil then
  begin
    Current := PChar(S);
    while Current^ <> #0 do
    begin
      case Current^ of
        '>': Inc(Integer(P));
        '<': Dec(Integer(P));
        '+': Inc(Integer(P^));
        '-': Dec(Integer(P^));
        '.': Write(Integer(P^));
        ',': Read(Integer(P^));
        '[': if Integer(P^) = 0 then
             begin
               repeat
                 Inc(Current);
               until Current^ = ']';
             end;
        ']': if Integer(P^) <> 0 then
             begin
               repeat
                 Dec(Current);
               until Pred(Current^) = '[';
             end;
        else raise EInvalidBFCommand.Create('Invalid character in BF source code: "' + Current^ + '"');
      end;
      Inc(Current);
    end;
  end;
end;

procedure BFInterpret(const S: String; var P: Pointer; var ReadBuffer, WriteBuffer: TIntegerArray);
{ Siehe Methode "BFInterpret(String,Pointer)".
  Eingaben werden aus "ReadBuffer" eingelesen und Ausgaben in "WriteBuffer"
  ausgegeben. }
var
  Current: PChar;
  Reader: PInteger;
begin
  if P <> nil then
  begin
    Current := PChar(S);
    Reader := PInteger(ReadBuffer);
    while Current^ <> #0 do
    begin
      case Current^ of
        '>': Inc(Integer(P));
        '<': Dec(Integer(P));
        '+': Inc(Integer(P^));
        '-': Dec(Integer(P^));
        '.': begin
               SetLength(WriteBuffer,Length(WriteBuffer) + 1);
               WriteBuffer[Length(WriteBuffer) - 1] := Integer(P^);
             end;
        ',': begin
               Integer(P^) := Reader^;
               Inc(Reader);
             end;
        '[': if Integer(P^) = 0 then
             begin
               repeat
                 Inc(Current);
               until Current^ = ']';
             end;
        ']': if Integer(P^) <> 0 then
             begin
               repeat
                 Dec(Current);
               until Pred(Current^) = '[';
             end;
        else raise EInvalidBFCommand.Create('Invalid character in BF source code: "' + Current^ + '"');
      end;
      Inc(Current);
    end;
  end;
end;

procedure ExtractChars(var Text: String; Chars: array of Char); overload;
var
  Current: PChar;
  OutPut: String;
begin
  Current := PChar(Text);
  SetLength(OutPut,0);
  while Current^ <> #0 do
  begin
    if ArrayPos(Current^,Chars) <> -1 then
    begin
      OutPut := OutPut + Current^;
    end;
    Inc(Current);
  end;
  Text := OutPut;
end;

procedure ExtractChars(var Text: String; Chars: TCharSet); overload;
var
  Current: PChar;
  OutPut: String;
begin
  Current := PChar(Text);
  SetLength(OutPut,0);
  while Current^ <> #0 do
  begin
    if Current^ in Chars then
    begin
      OutPut := OutPut + Current^;
    end;
    Inc(Current);
  end;
  Text := OutPut;
end;

procedure SetPrivilege(const Name: PChar; Value: Boolean); overload;
var
  Token: THandle;
  Privileges: TTokenPrivileges;
begin
  if OpenProcessToken(GetCurrentProcess(),TOKEN_ADJUST_PRIVILEGES,Token) then
  begin
    Privileges.PrivilegeCount := 1;
    LookupPrivilegeValue(nil,Name,Privileges.Privileges[0].Luid);
    if Value = True then
    begin
      Privileges.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;
    end else
    begin
      Privileges.Privileges[0].Attributes := 0;
    end;
    AdjustTokenPrivileges(Token,False,Privileges,SizeOf(Privileges),nil,DWord(nil^));
    CloseHandle(Token);
  end;
end;

procedure SetPrivilege(const Name: String; Value: Boolean); overload;
begin
  SetPrivilege(PChar(Name),Value);
end;

function WinUserName: String;
var
  Buffer: array [0..255] of Char;
  Size: DWord;
begin
  Size := SizeOf(Buffer);
  if not GetUserName(Buffer, Size) then
  begin
    raise EWinUserInformation.Create('Could not collect information on user name');
  end;
  SetString(Result,Buffer,Size - 1);
end;

function WinUserDirectory: String;
begin
  Result := GetEnvironmentVariable('USERPROFILE');
end;

function WinUserAdmin: Boolean;
const
  SECURITY_NT_AUTHORITY: TSIDIdentifierAuthority = (Value: (0,0,0,0,0,5));
  SECURITY_BUILTIN_DOMAIN_RID = $00000020;
  DOMAIN_ALIAS_RID_ADMINS = $00000220;
var
  hAccessToken: THandle;
  ptgGroups: PTokenGroups;
  dwInfoBufferSize: DWORD;
  psidAdministrators: PSID;
  X: Integer;
  bSuccess: BOOL;
begin
  Result := False;
  bSuccess := False;
  ptgGroups := nil;
  psidAdministrators := nil;
  try
    bSuccess := OpenThreadToken(GetCurrentThread,TOKEN_QUERY,True,hAccessToken);
    if not bSuccess then
    begin
      if GetLastError = ERROR_NO_TOKEN then
      begin
        bSuccess := OpenProcessToken(GetCurrentProcess,TOKEN_QUERY,hAccessToken);
      end;
    end;
    if bSuccess then
    begin
      GetMem(ptgGroups,1024);
      bSuccess := GetTokenInformation(hAccessToken,TokenGroups,ptgGroups,1024,dwInfoBufferSize);
      if bSuccess then
      begin
        AllocateAndInitializeSid(SECURITY_NT_AUTHORITY,2,SECURITY_BUILTIN_DOMAIN_RID,DOMAIN_ALIAS_RID_ADMINS,0,0,0,0,0,0,psidAdministrators);
        {$R-}
        for X := 0 to ptgGroups.GroupCount - 1 do
        begin
          if EqualSid(psidAdministrators,ptgGroups.Groups[X].Sid) then
          begin
            Result := True;
            Break;
          end;
        end;
        {$R+}
      end;
    end;
  finally
    if bSuccess then
    begin
      CloseHandle(hAccessToken);
    end;
    if Assigned(ptgGroups) then
    begin
      FreeMem(ptgGroups);
    end;
    if Assigned(psidAdministrators) then
    begin
      FreeSid(psidAdministrators);
    end;
  end;
end;

function WinUserExists(UsrNme: String): Boolean;
begin
  Result := False;
  //...   MUSS NOCH GESCHRIEBEN WERDEN!!!
end;

{ ----------------------------------------------------------------------------
  TCycle
  ---------------------------------------------------------------------------- }

constructor TCycle.Create;
begin
  inherited;
  FRadius := 0;
end;

destructor TCycle.Destroy;
begin
  //...
  inherited;
end;

function TCycle.GetDiameter: Extended;
begin
  Result := Radius * 2;
end;

procedure TCycle.SetDiameter(Value: Extended);
begin
  Radius := Value / 2;
end;

function TCycle.GetCircumference: Extended;
begin
  Result := 2 * Pi * Radius;
end;

procedure TCycle.SetCircumference(Value: Extended);
begin
  FRadius := Value / 2 / Pi;
end;

{ ----------------------------------------------------------------------------
  TRange
  ---------------------------------------------------------------------------- }

constructor TRange.Create;
begin
  inherited;
  FOffset := 0;
  FTarget := 0;
  FStep := 1;
end;

destructor TRange.Destroy;
begin
  //...
  inherited;
end;

procedure TRange.SetOffset(Value: Integer);
begin
  if Value <= Target then
  begin
    FOffset := Value;
  end;
end;

procedure TRange.SetTarget(Value: Integer);
begin
  if Value >= Offset then
  begin
    FTarget := Value;
  end;
end;

function TRange.GetLength: Integer;
begin
  Result := Target - Offset;
end;

procedure TRange.SetLength(Value: Integer);
begin
  if Value >= 0 then
  begin
    Length := Value;
  end;
end;

function TRange.GetCount: Integer;
begin
  Result := Length div Step;
end;

function TRange.GetElements: TIntegerArray;
var
  Index: Integer;
begin
  System.SetLength(Result,Count);
  for Index := 0 to System.Length(Result) - 1 do
  begin
    Result[Index] := Offset + Step * Index;
  end;
end;

{ ----------------------------------------------------------------------------
  TFilteredStringList
  ---------------------------------------------------------------------------- }

constructor TFilteredStringList.Create;
begin
  inherited;
  FFilteredStrings := TStringList.Create;
  FFiltered := True;
  FFilterMode := lpBeginning;
  FFilterOptions := [sfoCaseSensitive,sfoForceTrim,sfoDefaultVisible];
end;

destructor TFilteredStringList.Destroy;
begin
  FFilteredStrings.Free;
  inherited;
end;

function TFilteredStringList.GetFilteredStrings: TStrings;
begin
  if Filtered = True then
  begin
    FilterUpdate;
    Result := FFilteredStrings;
  end else
  begin
    Result := Self;
  end;
end;

procedure TFilteredStringList.FilterUpdate;
var
  Index: Integer;
  StringPos: Integer;
  FilterValue: String;
  StringValue: String;
begin
  FFilteredStrings.Clear;
  for Index := 0 to Count - 1 do
  begin
    FilterValue := Filter;
    StringValue := Strings[Index];
    if not (sfoCaseSensitive in FilterOptions) then
    begin
      FilterValue := LowerCase(FilterValue);
      StringValue := LowerCase(StringValue);
    end;
    if (sfoForceTrim in FilterOptions) then
    begin
      FilterValue := Trim(FilterValue);
      StringValue := Trim(StringValue);
    end;
    StringPos := Pos(FilterValue,StringValue);
    if (((FilterMode = lpBeginning) and (StringPos = 1)) or
       ((FilterMode = lpAnyPosition) and (StringPos > 0))) or
       ((Length(FilterValue) = 0) and (sfoDefaultVisible in FilterOptions)) then
    begin
      FFilteredStrings.Add(Strings[Index]);
    end;
  end;
end;

end.
