# Lina Components 

These components were created by Dennis Göhlert.

origin: https://bitbucket.org/Dennis07/lina-components/src

There are some videos, demos at:

https://www.youtube.com/channel/UC68AZlDOXbm2EqCTDiKRMyA

For some years the following description was in the Google Code project:

Lina Components is a components suite and a library of classes, objects and code. These elements are made to simplify the progress of developing complex applications for different deep usability. Currently 5 new non-visual components are included. There exists a compatibility version which can run under almost any Delphi version. However, current development will only be done with compatibility for Delphi 2009 and higher. The code is being optimized for Delphi XE5.